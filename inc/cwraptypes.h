﻿/**
  \author  Igor Rusakov, 2018-2019
*/
#ifndef _CWRAPTYPES_H_
#define _CWRAPTYPES_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef char  Guid[48];

typedef double MatrixDbl3by3[3][3];

typedef MatrixDbl3by3 T_inertia;

/** EArrayDbl одномерный массив double */
typedef struct {
  double        *values;
  size_t          n;
} EArrayDbl;

/** ETableDbl двумерный массив double */
typedef struct {
  double        *table;
  size_t  rows;
  size_t  columns;
} ETableDbl;

/** ERank3ArrayDbl трёх мерный массив double */
typedef struct {
  double        *table;
  size_t  rows;
  size_t  columns;
  size_t  pages;
} ERank3ArrayDbl;

/** Массив символов (строка) для С API Fortran */
typedef struct EString {
  char    *text;  // one-byte character array
  size_t  len;    // size of array in characters
  size_t  bytes;  // size of array in bytes
} EString;

/** UnitsCombiTable Единицы измерения табличной функции двух аргументов */
typedef struct {
  EString   arg1;
  EString   arg2;
  EString   table;
} UnitsCombiTable;

/** UnitsCombi3Table единицы измерения табличной функции трёх аргументов */
typedef struct {
  EString   arg1;
  EString   arg2;
  EString   arg3;
  EString   table;
} UnitsCombi3Table;

/** UnitsPtrCombiTable единицы измерения табличной функции двух аргументов из указателей
 Желательно не использовать этот тип, только в случае крайней необходимости */
typedef struct {
  EString   *arg1;
  EString   *arg2;
  EString   *table;
} UnitsPtrCombiTable;

/** UnitsInertia Единицы измерения инерции */
typedef struct {
  EString inertia;
} UnitsInertia;

/** EPoly2Dbl Полином второго порядка в виде коэффициентов */
typedef struct {
  double  a0;
  double  a1;
  double  a2;
} EPoly2Dbl;

/** CombiTableDbl Табличная зависимость функции двух аргументов 
    Структура для вычислений */
typedef struct {
  EString         smoothness;
  EString         beyond;
  EArrayDbl       arg1;
  EString         arg1_title;
  EArrayDbl       arg2;
  EString         arg2_title;
  ETableDbl       table;
  EString         title;
  UnitsCombiTable units;
} CombiTableDbl;

/** Combi3TableDbl Табличная зависимость функции трёх аргументов  */
typedef struct {
  EString         smoothness;
  EString         beyond;
  EArrayDbl       arg1;
  EString         arg1_title;
  EArrayDbl       arg2;
  EString         arg2_title;
  EArrayDbl       arg3;
  EString         arg3_title;
  ERank3ArrayDbl  table;
  EString         title;
  UnitsCombi3Table  units;
} Combi3TableDbl;

/** UnitsPosition единицы измерения положения T_Position */
typedef struct {
  EString   r_0;
  EString   rotation;
} UnitsPosition;

typedef struct {
  double  r_0[3];
  double  rotation[3];
  EString rotation_type;
  EString rotation_sequence;
  UnitsPosition   units;
} T_position;

typedef struct
{
    EString factor;
} UnitsTwist;

typedef struct {
  double  factor;
  EString type;
  //EString units;
  UnitsTwist units;
} T_factor;

/** Компоненты вектора, заданные табличными зависимостями от одних и тех же аргументов.

    Применяется, например, для задания табличных зависимостей векторов.
*/
#define PRESNC_VECTORCOMBITABLE_X   0x1
#define PRESNC_VECTORCOMBITABLE_Y   0x2
#define PRESNC_VECTORCOMBITABLE_Z   0x4
typedef struct {
  char presnc;
  CombiTableDbl X;
  CombiTableDbl Y;
  CombiTableDbl Z;
} T_VectorCombiTableDbl;

typedef struct {
  EString   r_0;
  EString   length;
  EString   chord07;
  EString   mass;
  EString   gravity_moment;
  EString   inertia;
  EString   tiploss_factor;
  EString   alpha_0;
  EString   lift_curve_slope;
  EString   lift_stall;
  EString   lock_number;
  EString   drag;
  EString   induction;
  EString   profile;
} UnitsBladeSimple;

#define PRESNC_BLADESIMPLE_FLEX             0x1
#define PRESNC_BLADESIMPLE_R0               0x2
#define PRESNC_BLADESIMPLE_LENGTH           0x4
#define PRESNC_BLADESIMPLE_CHORD07          0x8
#define PRESNC_BLADESIMPLE_MASS             0x10
#define PRESNC_BLADESIMPLE_GRAVITY_MOMENT   0x20
#define PRESNC_BLADESIMPLE_INERTIA          0x40
#define PRESNC_BLADESIMPLE_TWIST            0x80
#define PRESNC_BLADESIMPLE_TIPLOSSFACTOR    0x100
#define PRESNC_BLADESIMPLE_LIFTCURVESLOPE   0x200
#define PRESNC_BLADESIMPLE_LIFTSTALL        0x400
#define PRESNC_BLADESIMPLE_ALPHA0           0x800
#define PRESNC_BLADESIMPLE_LOCKNUMBER       0x1000
#define PRESNC_BLADESIMPLE_DRAG             0x2000
#define PRESNC_BLADESIMPLE_INDUCTION        0x4000
#define PRESNC_BLADESIMPLE_PROFILE          0x8000

typedef struct {
  unsigned int  presnc;
  short     flex_type;
  double    r_0;
  double    length;
  double    chord07;
  double    mass;
  double    gravity_moment;
  double    inertia;
  T_factor  twist;
  double    tiploss_factor;
  double    lift_curve_slope;
  double    lift_stall;
  double    alpha_0;
  double    lock_number;
  double    drag;
  double    induction;
  double    profile;
  UnitsBladeSimple  units;
} T_param_blade_simple;

typedef struct {
  int           n;
  double        rpm_pace_accel;
  double        rpm_pace_decel;
  double        t1;
  double        t2;
  double        om1;
  double        om2;
  EPoly2Dbl     poly2_rpm_power;
  CombiTableDbl power_hbar_temp_tbl;
} T_param_enginewithregulator;

typedef struct {
  EString power_trans_limit;
  EString power_board_losses;
  EString power_main_use;
  EString power_tail_use;
} UnitsTransmission1M1T;

typedef struct {
  double  power_trans_limit;
  double  power_board_losses;
  double  power_main_use;
  double  power_tail_use;
  UnitsTransmission1M1T units;
} T_param_transmission_1m1t_rotor;

typedef struct {
  EString stop;
  EString angle;
  EString L_offset;
  EString damping;
  EString stiffness;
} UnitsHingeGeneral;

#define HINGE_FIX        0x1
#define HINGE_STOPMIN    0x2
#define HINGE_STOPMAX    0x4
#define HINGE_ANGLE0     0x8
#define HINGE_LOFFSET    0x10
#define HINGE_DAMPING    0x20
#define HINGE_STIFFNESS  0x40

#define FIXCOND_NULL        0
#define FIXCOND_ABS_RIGID   1
#define FIXCOND_ELASTIC     2

typedef struct {
  short     presnc;
  short     fix_cond;
  double    stop_min;
  double    stop_max;
  double    angle_0;
  double    L_offset;
  double    damping;
  double    stiffness;
  UnitsHingeGeneral units;
} T_param_hinge_general;

#define HUBSIMLPE_INERTIA       0x1
#define HUBSIMLPE_FLAP_HINGE    0x2
#define HUBSIMPLE_LAG_HINGE     0x4 
#define HUBSIMPLE_FEATHER_HINGE 0x8

typedef struct {
  short                     presnc;
  short                     num_blades;
  double                    inertia;
  T_param_hinge_general     flap_hinge;
  T_param_hinge_general     lag_hinge;
  T_param_hinge_general     feather_hinge;
  UnitsInertia              units;
} T_param_hub_simple;

typedef struct {
  EString d1;
  EString d2;
  EString lon_max;
  EString lon_min;
  EString lat_max;
  EString lat_min;
} UnitsSwashplate;

typedef struct {
  double    d1;
  double    d2;
  double    lon_max;
  double    lon_min;
  double    lat_max;
  double    lat_min;
  UnitsSwashplate units;
} T_param_swashplate;

typedef struct {
  Combi3TableDbl  ct_0;
  Combi3TableDbl  ct_oz;
  Combi3TableDbl  ct_ox;

  Combi3TableDbl  ch_0;
  Combi3TableDbl  ch_oz;
  Combi3TableDbl  ch_ox;

  Combi3TableDbl  cs_0;
  Combi3TableDbl  cs_oz;
  Combi3TableDbl  cs_ox;

  Combi3TableDbl  cmx_0;
  Combi3TableDbl  cmx_oz;
  Combi3TableDbl  cmx_ox;

  Combi3TableDbl  cmz_0;
  Combi3TableDbl  cmz_oz;
  Combi3TableDbl  cmz_ox;

  Combi3TableDbl  cmk_0;
  Combi3TableDbl  cmk_oz;
  Combi3TableDbl  cmk_ox;
  Combi3TableDbl  cmk_om;

  Combi3TableDbl  tr_ct;
  Combi3TableDbl  tr_ch;
  Combi3TableDbl  tr_cs;
  Combi3TableDbl  tr_cmk;
  double Omg_con;
} T_param_cet_1m1t_rotor;

typedef struct {
  EString   direction;
  EString   r;
  EString   pitch_flap_coupling;
  EString   omega_nom;
  EString   omega_min;
  EString   omega_ratio;
} UnitsRotorSimple;

#define PRESNC_ROTORSIMPLE_DIRECTION          0x1
#define PRESNC_ROTORSIMPLE_R                  0x2
#define PRESNC_ROTORSIMPLE_PITCHFLAPCOUPLING  0x4
#define PRESNC_ROTORSIMPLE_OMEGA_NOM          0x8
#define PRESNC_ROTORSIMPLE_OMEGA_MIN          0x10
#define PRESNC_ROTORSIMPLE_OMEGA_RATIO        0x20
#define PRESNC_ROTORSIMPLE_MEANPROFILEDRAG    0x40
#define PRESNC_ROTORSIMPLE_SWASHPLATE         0x80

typedef struct {
  short                 presnc;
  short                 direction;
  double                r;
  double                pitch_flap_coupling;
  double                omega_nom;
  double                omega_min;
  double                omega_ratio;
  EString               type;
  T_param_hub_simple    hub;
  T_param_blade_simple  blade;
  T_param_swashplate    swashplate;
  CombiTableDbl         mean_profile_drag;
  UnitsRotorSimple      units;
} T_param_rotor_simple;

typedef struct {
  EString span;
  EString v_shape;
  EString area;
} UnitsHTail;

#define PRESNC_HTAIL_SPAN         0x1
#define PRESNC_HTAIL_V_SHAPE      0x2
#define PRESNC_HTAIL_AREA         0x4
#define PRESNC_HTAIL_EXCENTR      0x8
#define PRESNC_HTAIL_C_PRESS      0x10
#define PRESNC_HTAIL_KINEMATICS   0x20
#define PRESNC_HTAIL_POSITION     0x40
#define PRESNC_HTAIL_AEROFORCE    0x80
#define PRESNC_HTAIL_AEROMOMENT   0x100

typedef struct {
  short   presnc;
  double  span;
  double  v_shape;
  double  area;
  double  excentr; 
  double  c_press[3];
  double  kinematics[3];
  T_position  position;
  T_VectorCombiTableDbl aero_force_coeffs;
  T_VectorCombiTableDbl aero_moment_coeffs;
  UnitsHTail  units;
} T_param_H_tail;

typedef struct {
  EString v_shape;
  EString area;
} UnitsVTail;

#define PRESNC_VTAIL_SYMMPLANE    0x1
#define PRESNC_VTAIL_V_SHAPE      0x2
#define PRESNC_VTAIL_AREA         0x4
#define PRESNC_VTAIL_AREA_FLOW    0x8
#define PRESNC_VTAIL_C_PRESS      0x10
#define PRESNC_VTAIL_BLANK01      0x20
#define PRESNC_VTAIL_POSITION     0x40
#define PRESNC_VTAIL_AEROFORCE    0x80
#define PRESNC_VTAIL_BLANK02      0x100

typedef struct {
  short presnc;
  short nsurfs;
  char symmplane[3];
  double  area;
  double  area_flow;
  double  v_shape;
  double  c_press[3];
  T_position  position;
  T_VectorCombiTableDbl aero_force_coeffs;
  UnitsVTail  units;
} T_param_V_tail;

typedef struct {
  EString length;
  EString cross_area;
  EString inertia;
} UnitsFuselageSimple;

typedef struct {
  double          length;
  double          cross_area;
  T_inertia       inertia;
  T_VectorCombiTableDbl aero_force_coeffs;
  T_VectorCombiTableDbl aero_moment_coeffs;
  UnitsFuselageSimple   units;
} T_param_fuselage_simple;

typedef struct {
  double          TOW;
  double          CG[3];
  T_param_fuselage_simple Body;
  T_param_rotor_simple    MainRotor;
  T_position              MainRotor_Position;
  T_param_rotor_simple    TailRotor;
  T_position              TailRotor_Position;
  T_param_transmission_1m1t_rotor Transmission;
  T_param_H_tail          HTail;
  T_param_V_tail          VTail;
  double                  OnBodyFlowLosses;
} T_param_simple_one_rotor_helicopter;

#ifdef __cplusplus
}
#endif

#endif // _CWRAPTYPES_H_
