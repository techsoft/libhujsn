#ifndef JDP_GLOBAL_H
#define JDP_GLOBAL_H

//#include <QtCore/qglobal.h>

#ifdef __GNUC__
#    define CDECL __attribute__ ((__cdecl__))
#  else
#    define CDECL __cdecl
#  endif

#if defined _WIN32 || defined __CYGWIN__ || defined __MINGW32__
  #ifdef JDP_LIBRARY
    #ifdef __GNUC__
      #define JDPSHARED_EXPORT __attribute__ ((dllexport))
    #else
      #define JDPSHARED_EXPORT __declspec(dllexport) // Note: actually gcc seems to also supports this syntax.
    #endif
  #else
    #ifdef __GNUC__
      #define JDPSHARED_EXPORT __attribute__ ((dllimport))
    #else
      #define JDPSHARED_EXPORT __declspec(dllimport) // Note: actually gcc seems to also supports this syntax.
    #endif
  #endif
//  #define DLL_LOCAL
#else
  #if __GNUC__ >= 4
    #define JDPSHARED_EXPORT __attribute__ ((visibility ("default")))
//    #define DLL_LOCAL  __attribute__ ((visibility ("hidden")))
  #else
    #define JDPSHARED_EXPORT
//    #define DLL_LOCAL
  #endif
#endif


//#if defined(JDP_LIBRARY)
//#  define JDPSHARED_EXPORT __declspec(dllexport) //Q_DECL_EXPORT
//#else
//#  define JDPSHARED_EXPORT __declspec(dllimport) //Q_DECL_IMPORT
//#endif

#endif // JDP_GLOBAL_H
