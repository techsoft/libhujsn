// mm3_hasher.cpp : Defines the entry point for the console application.
//
#include "rapidjson/filereadstream.h"
#include "rapidjson/document.h"
#include <iostream>
#include <fstream>
#include <rapidjson/istreamwrapper.h>
#include <unordered_set>
#include "MurmurHash3.h"

using namespace std; 
using namespace rapidjson;

std::unordered_set<std::string> keys = {
    "ct_0",
"ct_oz",
"ct_ox",
"ch_0",
"ch_oz",
"ch_ox",
"cs_0",
"cs_oz",
"cs_ox",
"cmx_0",
"cmx_oz",
"cmx_ox",
"cmz_0",
"cmz_oz",
"cmz_ox",
"cmk_0",
"cmk_oz",
"cmk_ox",
"cmk_om",
"tr_ct",
"tr_ch",
"tr_cs",
"tr_cmk",
"Omg_con",

"n",
"rpm_pace_accel",
"rpm_pace_decel",
"t1",
"t2",
"om1",
"om2",
"poly2_rpm_power",
"power_hbar_temp_tbl",

	"arg3",
	"arg3_title",

	"a0",
	"a1",
	"a2"
};

struct MyHandler
    : public BaseReaderHandler<UTF8<>, MyHandler> {

    bool Key(const char* str, SizeType length, bool copy) {
	keys.insert(std::string(str, length));
	return true;
    }

    bool Default() { return true; } // All other events are invalid.
};

int main(int argc, char**argv)
{

    std::ifstream input(argv[1], std::ios::binary);
       if (!input.is_open())
    return 1;
    BasicIStreamWrapper<std::ifstream> is(input);

    MyHandler handler;
    Reader reader;
    reader.Parse(is, handler);

    unordered_set<uint32_t> hashes;

    std::cout << "enum JsonKeyHashes {" << endl;
	 

    for (auto key : keys)
    {
	auto p = key.c_str();
	auto hash = MurmurHash3_x86_32(p, key.length(), 0x12345678);
	std::cout <<"\t" << key.c_str() << "_hash" << "= 0x" << std::hex << hash << "," << endl;
	    if(!hashes.insert(hash).second)
	    {
		std::cout << "DUPLICATE" << endl;
	    }
    }

    std::cout << "};" << endl;

    return 0;
}

