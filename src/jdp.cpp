﻿#include "jdp.h"
#include "cwraptypes.h"
#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"
#include "rapidjson/pointer.h"
#include "MurmurHash3.h"
#include "jsonkeyhashes.h"
#include <vector>
#include <unordered_set>
#include <map>
#include <iostream>

using namespace rapidjson;
using namespace std;

#define JUST_DO_IT(x) do {\
    Error::Code rrr = (x);\
    if(rrr != Error::NoError)\
	return rrr;\
} while(false)

//constexpr uint32_t PTO(rapidjson::Type t, uint32_t offset) {
//    return (t << 24 | (offset & 0xFFFFFF));
//}
#define PTO(t, offset) (uint32_t)((t&0xF) << 24 | (offset & 0xFFFFFF))
#define HAS_SPEC_HANLDER ((uint32_t)(1 << 31))
#define OPT_FIELD_FLAG ((uint32_t)(1 << 30))
constexpr inline uint32_t unpack_offset(uint32_t v) {
    return v & 0xFFFFFF;
}
constexpr inline JsonType::Enum unpack_type(uint32_t v) {
    return static_cast<JsonType::Enum>((v >> 24) & 0xF);
}

constexpr inline bool flags_has_spec_handler(uint32_t v) {
    return (v >> 31) & 1;
}

constexpr inline bool flags_is_opt_field(uint32_t v)
{
    return (v >> 30) & 1;
}

static const uint32_t mm3_seed = 0x12345678;

struct optvalue_t
{
    JsonKeyHashes hash;
    uint32_t bitpos;
};

struct field_t {
    void *handler;
    // hash of json field name
    JsonKeyHashes name;
    // offset of field in C struct
    // 31:is_spec_handler, 30:is_opt_field, 29-28: unused, 27-24:JsonType, 23-0:offset of field in struct
    uint32_t otf;
};

struct objinfo_t {
    field_t *fields;
    uint32_t field_count;

    field_t *Find(JsonKeyHashes hash) {
	for (unsigned i = 0; i < field_count; i++) {
	    if (fields[i].name == hash)
		return &fields[i];
	}
	return nullptr;
    }

    //void get_field_names(std::unordered_set<JsonKeyHashes> &names) const
    void get_field_names(std::unordered_set<uint32_t> &names) const
    {
	for (unsigned i = 0; i < field_count; i++)
	{
	    auto hash = fields[i].name;
	    if (hash == presnc_hash)
		continue;
	    names.insert(hash);
	}
    }
};
////////////////////////////////////////////////
struct Jdp;
typedef Error::Code(*field_handler_t)(Jdp *_this, Value &a, void* p_flddata);

Error::Code fixed_array_dbl_3x3_(Jdp *_this, Value &a, void*objinfo);
Error::Code fixed_array_dbl_1x3_(Jdp *_this, Value &a, void*objinfo);
Error::Code fixed_array_char_1x3_(Jdp *_this, Value &a, void*objinfo);
Error::Code ETblDbl_handler_(Jdp *_this, Value &a, void*objinfo);
Error::Code ER3ADbl_handler_(Jdp *_this, Value &a, void*objinfo);
////////////////////////////////////////////////
field_t EPoly2Dbl_obj_fields[] = {
    { nullptr,	a0_hash,    PTO(JsonType::Double, offsetof(EPoly2Dbl, a0)) },
{ nullptr,	a1_hash,    PTO(JsonType::Double, offsetof(EPoly2Dbl, a1)) },
{ nullptr,	a2_hash,    PTO(JsonType::Double, offsetof(EPoly2Dbl, a2)) },
};
objinfo_t EPoly2Dbl_obj_info = {
    EPoly2Dbl_obj_fields,
    sizeof(EPoly2Dbl_obj_fields) / sizeof(field_t)
};

field_t UnitsCombiTable_obj_fields[] = {
    { nullptr,	arg1_hash,  PTO(JsonType::String, offsetof(UnitsCombiTable, arg1)) },
{ nullptr,	arg2_hash,  PTO(JsonType::String, offsetof(UnitsCombiTable, arg2)) },
{ nullptr,	table_hash, PTO(JsonType::String, offsetof(UnitsCombiTable, table)) },
};
objinfo_t UnitsCT_o_i = {
    UnitsCombiTable_obj_fields,
    sizeof(UnitsCombiTable_obj_fields) / sizeof(field_t)
};
field_t CombiTableDbl_obj_fields[] = {
    { nullptr,		smoothness_hash,    PTO(JsonType::String, offsetof(CombiTableDbl, smoothness)) },
{ nullptr,		beyond_hash,	    PTO(JsonType::String, offsetof(CombiTableDbl, beyond)) },
{ nullptr,		arg1_hash,	    PTO(JsonType::Array, offsetof(CombiTableDbl, arg1)) },
{ nullptr,		arg1_title_hash,    PTO(JsonType::String, offsetof(CombiTableDbl, arg1_title)) },
{ nullptr,		arg2_hash,	    PTO(JsonType::Array, offsetof(CombiTableDbl, arg2)) },
{ nullptr,		arg2_title_hash,    PTO(JsonType::String, offsetof(CombiTableDbl, arg2_title)) },
{ reinterpret_cast<void*>(ETblDbl_handler_),	table_hash,	    HAS_SPEC_HANLDER | PTO(JsonType::Array, offsetof(CombiTableDbl, table)) },
{ nullptr,		title_hash,	    PTO(JsonType::String, offsetof(CombiTableDbl, title)) },
{ &UnitsCT_o_i,	units_hash,	    PTO(JsonType::Object, offsetof(CombiTableDbl, units)) },
};
objinfo_t CombiTableDbl_obj_info = {
    CombiTableDbl_obj_fields,
    sizeof(CombiTableDbl_obj_fields) / sizeof(field_t)
};

field_t UnitsCombi3Table_obj_fields[] = {
    { nullptr, arg1_hash, PTO(JsonType::String, offsetof(UnitsCombi3Table, arg1)) },
{ nullptr, arg2_hash, PTO(JsonType::String, offsetof(UnitsCombi3Table, arg2)) },
{ nullptr, arg3_hash, PTO(JsonType::String, offsetof(UnitsCombi3Table, arg3)) },
{ nullptr, table_hash, PTO(JsonType::String, offsetof(UnitsCombi3Table, table)) },
};
objinfo_t UnitsC3T_o_i = {
    UnitsCombi3Table_obj_fields,
    sizeof(UnitsCombi3Table_obj_fields) / sizeof(field_t)
};
field_t Combi3TableDbl_obj_fields[] = {
    { nullptr,		smoothness_hash,    PTO(JsonType::String, offsetof(Combi3TableDbl, smoothness)) },
{ nullptr,		beyond_hash,	    PTO(JsonType::String, offsetof(Combi3TableDbl, beyond)) },
{ nullptr,		arg1_hash,	    PTO(JsonType::Array, offsetof(Combi3TableDbl, arg1)) },
{ nullptr,		arg1_title_hash,    PTO(JsonType::String, offsetof(Combi3TableDbl, arg1_title)) },
{ nullptr,		arg2_hash,	    PTO(JsonType::Array, offsetof(Combi3TableDbl, arg2)) },
{ nullptr,		arg2_title_hash,    PTO(JsonType::String, offsetof(Combi3TableDbl, arg2_title)) },
{ nullptr,		arg3_hash,	    PTO(JsonType::Array, offsetof(Combi3TableDbl, arg3)) },
{ nullptr,		arg3_title_hash,    PTO(JsonType::String, offsetof(Combi3TableDbl, arg3_title)) },
{ reinterpret_cast<void*>(ER3ADbl_handler_),	table_hash,	    HAS_SPEC_HANLDER | PTO(JsonType::Array, offsetof(Combi3TableDbl, table)) },
{ nullptr,		title_hash,	    PTO(JsonType::String, offsetof(Combi3TableDbl, title)) },
{ &UnitsC3T_o_i,	units_hash,	    PTO(JsonType::Object, offsetof(Combi3TableDbl, units)) },
};
objinfo_t Combi3TableDbl_obj_info = {
    Combi3TableDbl_obj_fields,
    sizeof(Combi3TableDbl_obj_fields) / sizeof(field_t)
};

field_t UnitsPosition_ojb_fields[] = {
    { nullptr,	r_0_hash,	PTO(JsonType::String,offsetof(UnitsPosition, r_0)) },
{ nullptr,	rotation_hash,	PTO(JsonType::String,offsetof(UnitsPosition, rotation)) }
};
objinfo_t UnitsPosition_obj_info = {
    UnitsPosition_ojb_fields,
    sizeof(UnitsPosition_ojb_fields) / sizeof(field_t)
};
field_t T_position_obj_fields[] = {
    { reinterpret_cast<void*>(fixed_array_dbl_1x3_),		r_0_hash,		HAS_SPEC_HANLDER | PTO(JsonType::Array,offsetof(T_position, r_0)) },
{ reinterpret_cast<void*>(fixed_array_dbl_1x3_),		rotation_hash,		HAS_SPEC_HANLDER | PTO(JsonType::Array,offsetof(T_position, rotation)) },
{ nullptr,			rotationType_hash,	PTO(JsonType::String,offsetof(T_position, rotation_type)) },
{ nullptr,			rotationSequence_hash,	PTO(JsonType::String,offsetof(T_position, rotation_sequence)) },
{ &UnitsPosition_obj_info,	_ref_units__hash,	PTO(JsonType::String,offsetof(T_position, units)) },
};
objinfo_t T_position_obj_info = {
    T_position_obj_fields,
    sizeof(T_position_obj_fields) / sizeof(field_t)
};

field_t UnitsTwist_obj_fields[] = {
    { nullptr,	factor_hash, PTO(JsonType::String,offsetof(UnitsTwist, factor)) }
};
objinfo_t UnitsTwist_obj_info = {
    UnitsTwist_obj_fields,
    sizeof(UnitsTwist_obj_fields) / sizeof(field_t)
};
field_t T_factor_obj_fields[] = {
    { nullptr,			factor_hash,	    PTO(JsonType::Double,offsetof(T_factor, factor)) },
{ nullptr,			type_hash,	    PTO(JsonType::String,offsetof(T_factor, type)) },
{ &UnitsTwist_obj_info,	_ref_units__hash,   PTO(JsonType::String,offsetof(T_factor, units)) },
};
objinfo_t T_factor_obj_info = {
    T_factor_obj_fields,
    sizeof(T_factor_obj_fields) / sizeof(field_t)
};

optvalue_t T_VectorCTDbl_o_ov[] = {
    { X_hash,	0 },
{ Y_hash,	1 },
{ Z_hash,	2 },
{noname_hash, uint32_t(-1)}
};
field_t T_VectorCombiTableDbl_obj_fields[] = {
    { &T_VectorCTDbl_o_ov,	presnc_hash,	PTO(JsonType::Uint8,offsetof(T_VectorCombiTableDbl, presnc)) },
{ &CombiTableDbl_obj_info,	X_hash,		OPT_FIELD_FLAG | PTO(JsonType::Object,offsetof(T_VectorCombiTableDbl, X)) },
{ &CombiTableDbl_obj_info,	Y_hash,		OPT_FIELD_FLAG | PTO(JsonType::Object,offsetof(T_VectorCombiTableDbl, Y)) },
{ &CombiTableDbl_obj_info,	Z_hash,		OPT_FIELD_FLAG | PTO(JsonType::Object,offsetof(T_VectorCombiTableDbl, Z)) },
};
objinfo_t T_VectorCTDbl_obj_info = {
    T_VectorCombiTableDbl_obj_fields,
    sizeof(T_VectorCombiTableDbl_obj_fields) / sizeof(field_t)
};

field_t UnitsBladeSimple_obj_fields[] = {
    { nullptr,	r_0_hash,		PTO(JsonType::String,offsetof(UnitsBladeSimple, r_0)) },
{ nullptr,	Length_hash,		PTO(JsonType::String,offsetof(UnitsBladeSimple, length)) },
{ nullptr,	Chord07_hash,		PTO(JsonType::String,offsetof(UnitsBladeSimple, chord07)) },
{ nullptr,	Mass_hash,		PTO(JsonType::String,offsetof(UnitsBladeSimple, mass)) },
{ nullptr,	GravityMoment_hash,	PTO(JsonType::String,offsetof(UnitsBladeSimple, gravity_moment)) },
{ nullptr,	Inertia_hash,		PTO(JsonType::String,offsetof(UnitsBladeSimple, inertia)) },
{ nullptr,	TipLossFactor_hash,	PTO(JsonType::String,offsetof(UnitsBladeSimple, tiploss_factor)) },
{ nullptr,	alpha_0_hash,		PTO(JsonType::String,offsetof(UnitsBladeSimple, alpha_0)) },
{ nullptr,	LiftCurveSlope_hash,	PTO(JsonType::String,offsetof(UnitsBladeSimple, lift_curve_slope)) },
{ nullptr,	LiftStall_hash,		PTO(JsonType::String,offsetof(UnitsBladeSimple, lift_stall)) },
{ nullptr,	LockNumber_hash,	PTO(JsonType::String,offsetof(UnitsBladeSimple, lock_number)) },
{ nullptr,	Drag_hash,		PTO(JsonType::String,offsetof(UnitsBladeSimple, drag)) },
{ nullptr,	Induction_hash,		PTO(JsonType::String,offsetof(UnitsBladeSimple, induction)) },
{ nullptr,	Profile_hash,		PTO(JsonType::String,offsetof(UnitsBladeSimple, profile)) },
};
objinfo_t UnitsBladeSimple_obj_info = {
    UnitsBladeSimple_obj_fields,
    sizeof(UnitsBladeSimple_obj_fields) / sizeof(field_t)
};
optvalue_t T_param_blade_o_optvalues[] = {
    { flex_type_hash,	    0 },
{ r_0_hash,		    1 },
{ Length_hash,	    2 },
{ Chord07_hash,	    3 },
{ Mass_hash,		    4 },
{ GravityMoment_hash,    5 },
{ Inertia_hash,	    6 },
{ Twist_hash,	    7 },
{ TipLossFactor_hash,    8 },
{ LiftCurveSlope_hash,   9 },
{ LiftStall_hash,	    10 },
{ alpha_0_hash,	    11 },
{ LockNumber_hash,	    12 },
{ Drag_hash,		    13 },
{ Induction_hash,	    14 },
{ Profile_hash,	    15 },
{ noname_hash, uint32_t(-1) }
};
field_t T_param_blade_simple_obj_fields[] = {
    { T_param_blade_o_optvalues,    presnc_hash,	    PTO(JsonType::Uint32,offsetof(T_param_blade_simple, presnc)) },
{ nullptr,			    flex_type_hash,	    OPT_FIELD_FLAG | PTO(JsonType::Uint16,offsetof(T_param_blade_simple, flex_type)) },
{ nullptr,			    r_0_hash,		    OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_blade_simple, r_0)) },
{ nullptr,			    Length_hash,	    OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_blade_simple, length)) },
{ nullptr,			    Chord07_hash,	    OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_blade_simple, chord07)) },
{ nullptr,			    Mass_hash,		    OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_blade_simple, mass)) },
{ nullptr,			    GravityMoment_hash,	    OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_blade_simple, gravity_moment)) },
{ nullptr,			    Inertia_hash,	    OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_blade_simple, inertia)) },
{ &T_factor_obj_info,	    Twist_hash,		    OPT_FIELD_FLAG | PTO(JsonType::Object,offsetof(T_param_blade_simple, twist)) },
{ nullptr,			    TipLossFactor_hash,	    OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_blade_simple, tiploss_factor)) },
{ nullptr,			    LiftCurveSlope_hash,    OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_blade_simple, lift_curve_slope)) },
{ nullptr,			    LiftStall_hash,	    OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_blade_simple, lift_stall)) },
{ nullptr,			    alpha_0_hash,	    OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_blade_simple, alpha_0)) },
{ nullptr,			    LockNumber_hash,	    OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_blade_simple, lock_number)) },
{ nullptr,			    Drag_hash,		    OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_blade_simple, drag)) },
{ nullptr,			    Induction_hash,	    OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_blade_simple, induction)) },
{ nullptr,			    Profile_hash,	    OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_blade_simple, profile)) },
{ &UnitsBladeSimple_obj_info,   _ref_units__hash,	    PTO(JsonType::String,offsetof(T_param_blade_simple, units)) },
};
objinfo_t T_param_blade_simple_obj_info = {
    T_param_blade_simple_obj_fields,
    sizeof(T_param_blade_simple_obj_fields) / sizeof(field_t)
};

field_t T_param_enginewithregulator_obj_fields[] = {
    { nullptr,			n_hash,			    PTO(JsonType::Uint32, offsetof(T_param_enginewithregulator, n)) },
{ nullptr,			rpm_pace_accel_hash,	    PTO(JsonType::Double, offsetof(T_param_enginewithregulator, rpm_pace_accel)) },
{ nullptr,			rpm_pace_decel_hash,	    PTO(JsonType::Double, offsetof(T_param_enginewithregulator, rpm_pace_decel)) },
{ nullptr,			t1_hash,		    PTO(JsonType::Double, offsetof(T_param_enginewithregulator, t1)) },
{ nullptr,			t2_hash,		    PTO(JsonType::Double, offsetof(T_param_enginewithregulator, t2)) },
{ nullptr,			om1_hash,		    PTO(JsonType::Double, offsetof(T_param_enginewithregulator, om1)) },
{ nullptr,			om2_hash,		    PTO(JsonType::Double, offsetof(T_param_enginewithregulator, om2)) },
{ &EPoly2Dbl_obj_info,	poly2_rpm_power_hash,	    PTO(JsonType::Object, offsetof(T_param_enginewithregulator, poly2_rpm_power)) },
{ &CombiTableDbl_obj_info,	power_hbar_temp_tbl_hash,   PTO(JsonType::Object, offsetof(T_param_enginewithregulator, power_hbar_temp_tbl)) },
};
objinfo_t T_param_enginewithregulator_obj_info = {
    T_param_enginewithregulator_obj_fields,
    sizeof(T_param_enginewithregulator_obj_fields) / sizeof(field_t)
};

field_t UnitsTransmission1M1T_obj_fields[] = {
    { nullptr,	    PowerLimit_hash,	    PTO(JsonType::String,offsetof(UnitsTransmission1M1T, power_trans_limit)) },
{ nullptr,	    PowerBoardLosses_hash,  PTO(JsonType::String,offsetof(UnitsTransmission1M1T, power_board_losses)) },
{ nullptr,	    PowerMainUse_hash,	    PTO(JsonType::String,offsetof(UnitsTransmission1M1T, power_main_use)) },
{ nullptr,	    PowerTailUse_hash,	    PTO(JsonType::String,offsetof(UnitsTransmission1M1T, power_tail_use)) },
};
objinfo_t UnitsTransmiss_obj_info = {
    UnitsTransmission1M1T_obj_fields,
    sizeof(UnitsTransmission1M1T_obj_fields) / sizeof(field_t)
};
field_t T_param_transmission_1m1t_rotor_obj_fields[] = {
    { nullptr,			    PowerLimit_hash,	    PTO(JsonType::Double,offsetof(T_param_transmission_1m1t_rotor, power_trans_limit)) },
{ nullptr,			    PowerBoardLosses_hash,  PTO(JsonType::Double,offsetof(T_param_transmission_1m1t_rotor, power_board_losses)) },
{ nullptr,			    PowerMainUse_hash,	    PTO(JsonType::Double,offsetof(T_param_transmission_1m1t_rotor, power_main_use)) },
{ nullptr,			    PowerTailUse_hash,	    PTO(JsonType::Double,offsetof(T_param_transmission_1m1t_rotor, power_tail_use)) },
{ &UnitsTransmiss_obj_info,	    _ref_units__hash,	    PTO(JsonType::String,offsetof(T_param_transmission_1m1t_rotor, units)) },
};
objinfo_t T_param_transmission_obj_info = {
    T_param_transmission_1m1t_rotor_obj_fields,
    sizeof(T_param_transmission_1m1t_rotor_obj_fields) / sizeof(field_t)
};

field_t UnitsHingeGeneral_obj_fields[] = {
    { nullptr,	stop_hash,	PTO(JsonType::String,offsetof(UnitsHingeGeneral, stop)) },
{ nullptr,	angle_hash,	PTO(JsonType::String,offsetof(UnitsHingeGeneral, angle)) },
{ nullptr,	L_offset_hash,	PTO(JsonType::String,offsetof(UnitsHingeGeneral, L_offset)) },
{ nullptr,	damping_hash,	PTO(JsonType::String,offsetof(UnitsHingeGeneral, damping)) },
{ nullptr,	stiffness_hash, PTO(JsonType::String,offsetof(UnitsHingeGeneral, stiffness)) }
};
objinfo_t UnitsHingeGeneral_obj_info = {
    UnitsHingeGeneral_obj_fields,
    sizeof(UnitsHingeGeneral_obj_fields) / sizeof(field_t)
};
optvalue_t T_param_hinge_obj_opt_values[] = {
    { fix_cond_hash,	0 },
{ stop_min_hash,	1 },
{ stop_max_hash,	2 },
{ angle_0_hash,	3 },
{ L_offset_hash,	4 },
{ damping_hash,	5 },
{ stiffness_hash,	6 },
{ noname_hash, uint32_t(-1) }
};
field_t T_param_hinge_general_obj_fields[] = {
    { T_param_hinge_obj_opt_values, presnc_hash,	PTO(JsonType::Uint16,offsetof(T_param_hinge_general, presnc)) },
{ nullptr,			    fix_cond_hash,	OPT_FIELD_FLAG | PTO(JsonType::Uint16,offsetof(T_param_hinge_general, fix_cond)) },
{ nullptr,			    stop_min_hash,	OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_hinge_general, stop_min)) },
{ nullptr,			    stop_max_hash,	OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_hinge_general, stop_max)) },
{ nullptr,			    angle_0_hash,	OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_hinge_general, angle_0)) },
{ nullptr,			    L_offset_hash,	OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_hinge_general, L_offset)) },
{ nullptr,			    damping_hash,	OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_hinge_general, damping)) },
{ nullptr,			    stiffness_hash,	OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_hinge_general, stiffness)) },
{ &UnitsHingeGeneral_obj_info,  _ref_units__hash,	PTO(JsonType::String,offsetof(T_param_hinge_general, units)) },
};
objinfo_t T_param_hinge_general_obj_info = {
    T_param_hinge_general_obj_fields,
    sizeof(T_param_hinge_general_obj_fields) / sizeof(field_t)
};

field_t UnitsInertia_obj_fields[] = {
    { nullptr,	Inertia_hash,	PTO(JsonType::String,offsetof(UnitsInertia, inertia)) }
};
objinfo_t UnitsInertia_obj_info = {
    UnitsInertia_obj_fields,
    sizeof(UnitsInertia_obj_fields) / sizeof(field_t)
};
optvalue_t T_param_hub_simple_obj_values[] = {
    { Inertia_hash,	    0 },
{ Flap_hinge_hash,	    1 },
{ Lag_hinge_hash,	    2 },
{ Feather_hinge_hash,    3 },
{ noname_hash, uint32_t(-1) }
};
field_t T_param_hub_simple_obj_fields[] = {
    { T_param_hub_simple_obj_values,	presnc_hash,	    PTO(JsonType::Uint16,offsetof(T_param_hub_simple, presnc)) },
{ nullptr,				NumBlades_hash,	    PTO(JsonType::Uint16,offsetof(T_param_hub_simple, num_blades)) },
{ nullptr,				Inertia_hash,	    OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_hub_simple, inertia)) },
{ &T_param_hinge_general_obj_info,	Flap_hinge_hash,    OPT_FIELD_FLAG | PTO(JsonType::Object,offsetof(T_param_hub_simple, flap_hinge)) },
{ &T_param_hinge_general_obj_info,	Lag_hinge_hash,	    OPT_FIELD_FLAG | PTO(JsonType::Object,offsetof(T_param_hub_simple, lag_hinge)) },
{ &T_param_hinge_general_obj_info,	Feather_hinge_hash, OPT_FIELD_FLAG | PTO(JsonType::Object,offsetof(T_param_hub_simple, feather_hinge)) },
{ &UnitsInertia_obj_info,		_ref_units__hash,   PTO(JsonType::String,offsetof(T_param_hub_simple, units)) },
};
objinfo_t T_param_hub_simple_obj_info = {
    T_param_hub_simple_obj_fields,
    sizeof(T_param_hub_simple_obj_fields) / sizeof(field_t)
};

field_t UnitsSwashplate_obj_fields[] = {
    { nullptr,	D1_hash,	PTO(JsonType::String,offsetof(UnitsSwashplate, d1)) },
{ nullptr,	D2_hash,	PTO(JsonType::String,offsetof(UnitsSwashplate, d2)) },
{ nullptr,	lon_max_hash,	PTO(JsonType::String,offsetof(UnitsSwashplate, lon_max)) },
{ nullptr,	lon_min_hash,	PTO(JsonType::String,offsetof(UnitsSwashplate, lon_min)) },
{ nullptr,	lat_max_hash,	PTO(JsonType::String,offsetof(UnitsSwashplate, lat_max)) },
{ nullptr,	lat_min_hash,	PTO(JsonType::String,offsetof(UnitsSwashplate, lat_min)) }
};
objinfo_t UnitsSwashplate_obj_info = {
    UnitsSwashplate_obj_fields,
    sizeof(UnitsSwashplate_obj_fields) / sizeof(field_t)
};
field_t T_param_swashplate_obj_fields[] = {
    { nullptr,			D1_hash,	    PTO(JsonType::Double, offsetof(T_param_swashplate, d1)) },
{ nullptr,			D2_hash,	    PTO(JsonType::Double, offsetof(T_param_swashplate, d2)) },
{ nullptr,			lon_max_hash,	    PTO(JsonType::Double, offsetof(T_param_swashplate, lon_max)) },
{ nullptr,			lon_min_hash,	    PTO(JsonType::Double, offsetof(T_param_swashplate, lon_min)) },
{ nullptr,			lat_max_hash,	    PTO(JsonType::Double, offsetof(T_param_swashplate, lat_max)) },
{ nullptr,			lat_min_hash,	    PTO(JsonType::Double, offsetof(T_param_swashplate, lat_min)) },
{ &UnitsSwashplate_obj_info,	_ref_units__hash,   PTO(JsonType::String, offsetof(T_param_swashplate, units)) },
};
objinfo_t T_param_swashplate_obj_info = {
    T_param_swashplate_obj_fields,
    sizeof(T_param_swashplate_obj_fields) / sizeof(field_t)
};

field_t T_param_cet_1m1t_rotor_obj_fields[] = {

    { &Combi3TableDbl_obj_info,	ct_0_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, ct_0)) },
{ &Combi3TableDbl_obj_info, ct_oz_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, ct_oz)) },
{ &Combi3TableDbl_obj_info, ct_ox_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, ct_ox)) },

{ &Combi3TableDbl_obj_info, ch_0_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, ch_0)) },
{ &Combi3TableDbl_obj_info, ch_oz_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, ch_oz)) },
{ &Combi3TableDbl_obj_info, ch_ox_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, ch_ox)) },

{ &Combi3TableDbl_obj_info, cs_0_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, cs_0)) },
{ &Combi3TableDbl_obj_info, cs_oz_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, cs_oz)) },
{ &Combi3TableDbl_obj_info, cs_ox_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, cs_ox)) },

{ &Combi3TableDbl_obj_info, cmx_0_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, cmx_0)) },
{ &Combi3TableDbl_obj_info, cmx_oz_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, cmx_oz)) },
{ &Combi3TableDbl_obj_info, cmx_ox_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, cmx_ox)) },

{ &Combi3TableDbl_obj_info, cmz_0_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, cmz_0)) },
{ &Combi3TableDbl_obj_info, cmz_oz_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, cmz_oz)) },
{ &Combi3TableDbl_obj_info, cmz_ox_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, cmz_ox)) },

{ &Combi3TableDbl_obj_info, cmk_0_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, cmk_0)) },
{ &Combi3TableDbl_obj_info, cmk_oz_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, cmk_oz)) },
{ &Combi3TableDbl_obj_info, cmk_ox_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, cmk_ox)) },
{ &Combi3TableDbl_obj_info, cmk_om_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, cmk_om)) },

{ &Combi3TableDbl_obj_info, tr_ct_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, tr_ct)) },
{ &Combi3TableDbl_obj_info, tr_ch_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, tr_ch)) },
{ &Combi3TableDbl_obj_info, tr_cs_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, tr_cs)) },
{ &Combi3TableDbl_obj_info, tr_cmk_hash,	PTO(JsonType::Object, offsetof(T_param_cet_1m1t_rotor, tr_cmk)) },

{ nullptr,			Omg_con_hash,	PTO(JsonType::Double, offsetof(T_param_cet_1m1t_rotor, Omg_con)) },

};
objinfo_t T_param_cet_1m1t_rotor_obj_info = {
    T_param_cet_1m1t_rotor_obj_fields,
    sizeof(T_param_cet_1m1t_rotor_obj_fields) / sizeof(field_t)
};

field_t UnitsRotorSimple_obj_fields[] = {
    { nullptr,	Direction_hash,		PTO(JsonType::String,offsetof(UnitsRotorSimple, direction)) },
{ nullptr,	Radius_hash,		PTO(JsonType::String,offsetof(UnitsRotorSimple, r)) },
{ nullptr,	PitchFlapCoupling_hash,	PTO(JsonType::String,offsetof(UnitsRotorSimple, pitch_flap_coupling)) },
{ nullptr,	Omega_nom_hash,		PTO(JsonType::String,offsetof(UnitsRotorSimple, omega_nom)) },
{ nullptr,	Omega_min_hash,		PTO(JsonType::String,offsetof(UnitsRotorSimple, omega_min)) },
{ nullptr,	OmegaRatio_hash,	PTO(JsonType::String,offsetof(UnitsRotorSimple, omega_ratio)) }
};
objinfo_t UnitsRotorSimple_obj_info = {
    UnitsRotorSimple_obj_fields,
    sizeof(UnitsRotorSimple_obj_fields) / sizeof(field_t)
};
optvalue_t T_param_rotor_simple_o_values[] = {
    { Direction_hash,		0 },
{ Radius_hash,		1 },
{ PitchFlapCoupling_hash ,	2 },
{ Omega_nom_hash,		3 },
{ Omega_min_hash,		4 },
{ OmegaRatio_hash,		5 },
{ MeanProfileDrag_hash,	6 },
{ Swashplate_hash,		7 },
{ noname_hash, uint32_t(-1) }
};
field_t T_param_rotor_simple_obj_fields[] = {
    { T_param_rotor_simple_o_values,	presnc_hash,		PTO(JsonType::Uint16,offsetof(T_param_rotor_simple, presnc)) },
{ nullptr,				Direction_hash,		OPT_FIELD_FLAG | PTO(JsonType::Uint16,offsetof(T_param_rotor_simple, direction)) },
{ nullptr,				Radius_hash,		OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_rotor_simple, r)) },
{ nullptr,				PitchFlapCoupling_hash, OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_rotor_simple, pitch_flap_coupling)) },
{ nullptr,				Omega_nom_hash,		OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_rotor_simple, omega_nom)) },
{ nullptr,				Omega_min_hash,		OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_rotor_simple, omega_min)) },
{ nullptr,				OmegaRatio_hash,	OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_rotor_simple, omega_ratio)) },
{ nullptr,				Type_hash,		PTO(JsonType::String,offsetof(T_param_rotor_simple, type)) },
{ &T_param_hub_simple_obj_info,	Hub_hash,		PTO(JsonType::Object,offsetof(T_param_rotor_simple, hub)) },
{ &T_param_blade_simple_obj_info,	Blade_hash,		PTO(JsonType::Object,offsetof(T_param_rotor_simple, blade)) },
{ &T_param_swashplate_obj_info,	Swashplate_hash,	OPT_FIELD_FLAG | PTO(JsonType::Object,offsetof(T_param_rotor_simple, swashplate)) },
// special case handled directly in process_object ( DIRTY HACK !!! )
{ &CombiTableDbl_obj_info,		MeanProfileDrag_hash,	HAS_SPEC_HANLDER | OPT_FIELD_FLAG | PTO(JsonType::Object,offsetof(T_param_rotor_simple, mean_profile_drag)) },
{ &UnitsRotorSimple_obj_info,	_ref_units__hash,	PTO(JsonType::String,offsetof(T_param_rotor_simple, units)) },
};
objinfo_t T_param_rotor_simple_obj_info = {
    T_param_rotor_simple_obj_fields,
    sizeof(T_param_rotor_simple_obj_fields) / sizeof(field_t)
};

field_t UnitsHTail_obj_fields[] = {
    { nullptr,	Span_hash,	PTO(JsonType::String,offsetof(UnitsHTail, span)) },
{ nullptr,	V_shape_hash,	PTO(JsonType::String,offsetof(UnitsHTail, v_shape)) },
{ nullptr,	Area_hash,	PTO(JsonType::String,offsetof(UnitsHTail, area)) }
};
objinfo_t UnitsHTail_obj_info = {
    UnitsHTail_obj_fields,
    sizeof(UnitsHTail_obj_fields) / sizeof(field_t)
};
optvalue_t T_param_H_tail_obj_values[] = {
    { Span_hash,			0 },
{ V_shape_hash,		1 },
{ Area_hash,			2 },
{ Excentric_hash,		3 },
{ CenterOfPressure_hash,	4 },
{ Kinematics_hash,		5 },
{ Position_hash,		6 },
{ AeroForceCoeffs_hash,	7 },
{ AeroMomentCoeffs_hash,	8 },
{ noname_hash, uint32_t(-1) }
};
field_t T_param_H_tail_obj_fields[] = {
    { T_param_H_tail_obj_values,    presnc_hash,	    PTO(JsonType::Uint16,offsetof(T_param_H_tail, presnc)) },
{ nullptr,			    Span_hash,		    OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_H_tail, span)) },
{ nullptr,			    V_shape_hash,	    OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_H_tail, v_shape)) },
{ nullptr,			    Area_hash,		    OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_H_tail, area)) },
{ nullptr,			    Excentric_hash,	    OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_H_tail, excentr)) },
{ reinterpret_cast<void*>(fixed_array_dbl_1x3_),		    CenterOfPressure_hash,  OPT_FIELD_FLAG | HAS_SPEC_HANLDER | PTO(JsonType::Array,offsetof(T_param_H_tail, c_press)) },
{ reinterpret_cast<void*>(fixed_array_dbl_1x3_),		    Kinematics_hash,	    OPT_FIELD_FLAG | HAS_SPEC_HANLDER | PTO(JsonType::Array,offsetof(T_param_H_tail, kinematics)) },
{ &T_position_obj_info,	    Position_hash,	    OPT_FIELD_FLAG | PTO(JsonType::Object,offsetof(T_param_H_tail, position)) },
{ &T_VectorCTDbl_obj_info,	    AeroForceCoeffs_hash,   OPT_FIELD_FLAG | PTO(JsonType::Object,offsetof(T_param_H_tail, aero_force_coeffs)) },
{ &T_VectorCTDbl_obj_info,	    AeroMomentCoeffs_hash,  OPT_FIELD_FLAG | PTO(JsonType::Object,offsetof(T_param_H_tail, aero_moment_coeffs)) },
{ &UnitsHTail_obj_info,	    _ref_units__hash,	    PTO(JsonType::String,offsetof(T_param_H_tail, units)) },
};
objinfo_t T_param_H_tail_obj_info = {
    T_param_H_tail_obj_fields,
    sizeof(T_param_H_tail_obj_fields) / sizeof(field_t)
};


field_t UnitsVTail_obj_fields[] = {
    { nullptr,	V_shape_hash,	PTO(JsonType::String,offsetof(UnitsVTail, v_shape)) },
{ nullptr,	Area_hash,	PTO(JsonType::String,offsetof(UnitsVTail, area)) }
};
objinfo_t UnitsVTail_obj_info = {
    UnitsVTail_obj_fields,
    sizeof(UnitsVTail_obj_fields) / sizeof(field_t)
};
optvalue_t T_param_V_tail_obj_values[] = {
    { SymmetryPlane_hash,    0 },
{ V_shape_hash,	    1 },
{ Area_hash,		    2 },
{ AreaUnderFlow_hash,    3 },
{ CenterOfPressure_hash, 4 },
// BLANK01
{ Position_hash,	    6 },
{ AeroForceCoeffs_hash,  7 },
// BLANK02
{ noname_hash, uint32_t(-1) }
};
field_t T_param_V_tail_obj_fields[] = {
    { T_param_V_tail_obj_values,    presnc_hash,	    PTO(JsonType::Uint16,offsetof(T_param_V_tail, presnc)) },
{ nullptr,			    NSurfaces_hash,	    PTO(JsonType::Uint16,offsetof(T_param_V_tail, nsurfs)) },
{ reinterpret_cast<void*>(fixed_array_char_1x3_),	    SymmetryPlane_hash,	    OPT_FIELD_FLAG | HAS_SPEC_HANLDER | PTO(JsonType::String,offsetof(T_param_V_tail, symmplane)) },
{ nullptr,			    Area_hash,		    OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_V_tail, area)) },
{ nullptr,			    AreaUnderFlow_hash,	    OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_V_tail, area_flow)) },
{ nullptr,			    V_shape_hash,	    OPT_FIELD_FLAG | PTO(JsonType::Double,offsetof(T_param_V_tail, v_shape)) },
{ reinterpret_cast<void*>(fixed_array_dbl_1x3_),		    CenterOfPressure_hash,  OPT_FIELD_FLAG | HAS_SPEC_HANLDER | PTO(JsonType::Array,offsetof(T_param_V_tail, c_press)) },
{ &T_position_obj_info,	    Position_hash,	    OPT_FIELD_FLAG | PTO(JsonType::Object,offsetof(T_param_V_tail, position)) },
{ &T_VectorCTDbl_obj_info,	    AeroForceCoeffs_hash,   OPT_FIELD_FLAG | PTO(JsonType::Object,offsetof(T_param_V_tail, aero_force_coeffs)) },
{ &UnitsVTail_obj_info,	    _ref_units__hash,	    PTO(JsonType::String,offsetof(T_param_V_tail, units)) },
};
objinfo_t T_param_V_tail_obj_info = {
    T_param_V_tail_obj_fields,
    sizeof(T_param_V_tail_obj_fields) / sizeof(field_t)
};

field_t UnitsFuselageSimple_obj_fields[] = {
    { nullptr, Length_hash,	PTO(JsonType::String,offsetof(UnitsFuselageSimple, length)) },
{ nullptr, CrossArea_hash,  PTO(JsonType::String,offsetof(UnitsFuselageSimple, cross_area)) },
{ nullptr, Inertia_hash,    PTO(JsonType::String,offsetof(UnitsFuselageSimple, inertia)) },
};
objinfo_t UnitsFuselageSimple_obj_info = {
    UnitsFuselageSimple_obj_fields,
    sizeof(UnitsFuselageSimple_obj_fields) / sizeof(field_t)
};
field_t  T_param_fuselage_simple_obj_fields[] = {

    { nullptr,				Length_hash,		PTO(JsonType::Double,offsetof(T_param_fuselage_simple, length)) },
{ nullptr,				CrossArea_hash,		PTO(JsonType::Double,offsetof(T_param_fuselage_simple, cross_area)) },
{ reinterpret_cast<void*>(fixed_array_dbl_3x3_),		Inertia_hash,		HAS_SPEC_HANLDER | PTO(JsonType::Array,offsetof(T_param_fuselage_simple, inertia)) },
{ &T_VectorCTDbl_obj_info,		AeroForceCoeffs_hash,	PTO(JsonType::Object,offsetof(T_param_fuselage_simple, aero_force_coeffs)) },
{ &T_VectorCTDbl_obj_info,		AeroMomentCoeffs_hash,	PTO(JsonType::Object,offsetof(T_param_fuselage_simple, aero_moment_coeffs)) },
{ &UnitsFuselageSimple_obj_info,	_ref_units__hash,	PTO(JsonType::String,offsetof(T_param_fuselage_simple, units)) },
};
objinfo_t T_param_fuselage_obj_info = {
    T_param_fuselage_simple_obj_fields,
    sizeof(T_param_fuselage_simple_obj_fields) / sizeof(field_t)
};

field_t T_param_simple_one_rotor_helicopter_obj_fields[] = {
    { nullptr,				TakeOffWeight_hash,	    PTO(JsonType::Double, offsetof(T_param_simple_one_rotor_helicopter,TOW)) },
{ reinterpret_cast<void*>(fixed_array_dbl_1x3_),			CenterOfGravity_hash,	    HAS_SPEC_HANLDER | PTO(JsonType::Array, offsetof(T_param_simple_one_rotor_helicopter,CG)) },
{ &T_param_fuselage_obj_info,	Body_hash,		    PTO(JsonType::Object, offsetof(T_param_simple_one_rotor_helicopter,Body)) },
{ &T_param_rotor_simple_obj_info,	MainRotor_hash,		    PTO(JsonType::Object, offsetof(T_param_simple_one_rotor_helicopter,MainRotor)) },
{ &T_position_obj_info,		MainRotorPosition_hash,	    PTO(JsonType::Object, offsetof(T_param_simple_one_rotor_helicopter,MainRotor_Position)) },
{ &T_param_rotor_simple_obj_info,	TailRotor_hash,		    PTO(JsonType::Object, offsetof(T_param_simple_one_rotor_helicopter,TailRotor)) },
{ &T_position_obj_info,		TailRotorPosition_hash,	    PTO(JsonType::Object, offsetof(T_param_simple_one_rotor_helicopter,TailRotor_Position)) },
{ &T_param_transmission_obj_info,	Transmission_hash,	    PTO(JsonType::Object, offsetof(T_param_simple_one_rotor_helicopter,Transmission)) },
{ &T_param_H_tail_obj_info,		H_Tail_hash,		    PTO(JsonType::Object, offsetof(T_param_simple_one_rotor_helicopter,HTail)) },
{ &T_param_V_tail_obj_info,		V_Tail_hash,		    PTO(JsonType::Object, offsetof(T_param_simple_one_rotor_helicopter,VTail)) },
{ nullptr,				OnBodyFlowLosses_hash,	    PTO(JsonType::Double, offsetof(T_param_simple_one_rotor_helicopter,OnBodyFlowLosses)) }
};
objinfo_t T_param_simple_one_rotor_helicopter_obj_info = {
    T_param_simple_one_rotor_helicopter_obj_fields,
    sizeof(T_param_simple_one_rotor_helicopter_obj_fields) / sizeof(field_t)
};

// выровнять на 8-байтовую границу 
inline uint8_t* align_up_8(uint8_t* p)
{
    uint32_t dw = reinterpret_cast<uint32_t>(p);
    if (dw % 8)
    {
	p += 8 - (dw % 8);
    }
    return p;
}

constexpr uint32_t align_up_8(uint32_t dw)
{
    return (dw + 7) & ~7;
}

// FIXME: last char can be incomplete
size_t utf8len(const char *s, uint32_t bytes_count)
{
    unsigned k = 0;
    for(unsigned  i=0; i < bytes_count; i++)
	if((s[i] & 0xC0) != 0x80)
	    k++;

    return k;
}

std::map<TypeIds::Enum, std::pair<objinfo_t*, uint32_t> > tiso = {
//    { TypeIds::Unknown, 0},
    {TypeIds::param_one_rotor_helicopter, { &T_param_simple_one_rotor_helicopter_obj_info, (sizeof(T_param_simple_one_rotor_helicopter))} },

    {TypeIds::param_fuselage, {&T_param_fuselage_obj_info, (sizeof(T_param_fuselage_simple)) }  },
    {TypeIds::units_Fuselage, {&UnitsFuselageSimple_obj_info, (sizeof(UnitsFuselageSimple)) }  },

    { TypeIds::param_V_tail, {&T_param_V_tail_obj_info, (sizeof(T_param_V_tail)) }  },
    { TypeIds::units_VTail, {&UnitsVTail_obj_info, (sizeof(UnitsVTail)) }  },

    { TypeIds::param_H_tail, {&T_param_H_tail_obj_info, (sizeof(T_param_H_tail)) }  },
    { TypeIds::UnitsHTail, {&UnitsHTail_obj_info, (sizeof(UnitsHTail)) } },

    { TypeIds::param_rotor_simple, {&T_param_rotor_simple_obj_info, (sizeof(T_param_rotor_simple)) }  },
    { TypeIds::UnitsRotorSimple, {&UnitsRotorSimple_obj_info, (sizeof(UnitsRotorSimple)) } },

    { TypeIds::param_cet_1m1t_rotor, {&T_param_cet_1m1t_rotor_obj_info, (sizeof(T_param_cet_1m1t_rotor)) } },

    { TypeIds::param_swashplate, {&T_param_swashplate_obj_info, (sizeof(T_param_swashplate)) } },
    { TypeIds::UnitsSwashplate, {&UnitsSwashplate_obj_info, (sizeof(UnitsSwashplate)) } },

{ TypeIds::param_hub_simple, {&T_param_hub_simple_obj_info, (sizeof(T_param_hub_simple)) } },

{ TypeIds::param_hinge_general, {&T_param_hinge_general_obj_info, (sizeof(T_param_hinge_general)) } },
{ TypeIds::UnitsHingeGeneral, {&UnitsHingeGeneral_obj_info, (sizeof(UnitsHingeGeneral)) } },

{ TypeIds::param_transmission_1m1t_rotor, {&T_param_transmission_obj_info, (sizeof(T_param_transmission_1m1t_rotor)) } },
{ TypeIds::UnitsTransmission1M1T, {&UnitsTransmiss_obj_info, (sizeof(UnitsTransmission1M1T)) } },

{ TypeIds::param_enginewithregulator, {&T_param_enginewithregulator_obj_info, (sizeof(T_param_enginewithregulator)) } },

{ TypeIds::param_blade_simple, {&T_param_blade_simple_obj_info, (sizeof(T_param_blade_simple)) } },
{ TypeIds::UnitsBladeSimple, {&UnitsBladeSimple_obj_info, (sizeof(UnitsBladeSimple)) } },

{ TypeIds::VectorCombiTableDbl, {&T_VectorCTDbl_obj_info, (sizeof(T_VectorCombiTableDbl)) } },
{ TypeIds::T_factor, { &T_factor_obj_info, (sizeof(T_factor)) } },
{ TypeIds::T_position, {&T_position_obj_info, (sizeof(T_position)) } },
{ TypeIds::UnitsPosition, {&UnitsPosition_obj_info, (sizeof(UnitsPosition)) } },
{ TypeIds::Combi3TableDbl, {&Combi3TableDbl_obj_info, (sizeof(Combi3TableDbl)) } },
{ TypeIds::CombiTableDbl, {&CombiTableDbl_obj_info, (sizeof(CombiTableDbl)) } },
{ TypeIds::EPoly2Dbl, { &EPoly2Dbl_obj_info, (sizeof(EPoly2Dbl)) } },
{ TypeIds::UnitsInertia, {&UnitsInertia_obj_info, (sizeof(UnitsInertia)) } },
{ TypeIds::UnitsCombi3Table, {&UnitsC3T_o_i, (sizeof(UnitsCombi3Table)) } },
{ TypeIds::UnitsCombiTable, {&UnitsCT_o_i, (sizeof(UnitsCombiTable)) } },

};

std::map<Error::Code, const char*> error_strings = {
    {Error::Malloc_function_not_assigned, "malloc function not assigned. Look for set_alloc function."},
    {Error::Malloc_failed, "memory allocation request failed." },
{Error::DocumentIsNull, "internal document object is not initialized(null)" },
{Error::DocumentIsNotObject, "root json value type is not object" },
{Error::JsonTopLayoutMismatch, "root json object layout does not fit library requirements" },

{Error::MissingVersion, "root object does not contain version field" },
{Error::VersionIsNotString, "version field is not string" },
{Error::VersionMismatch, "version field value differs from 0.1.8" },

{Error::MissingUnitsObject, "root object does not contain _units_ field" },
{Error::UnitsIsNotObject, "_units_ field type is not object" },

{Error::MissingDataObject, "root object does not contain _data_ field" },
{Error::DataIsNotObject, "_data_ field type is not object" },

{Error::FileOpenError, "failed to open json file" },
{Error::DocumentParseError, "failed to parse json" },
{Error::TypeIdIsNotMapped, "no mapping for this type id." },
{Error::XpathPointsToNothing, "wrong xpath" },
{Error::XpathValueIsNotObject, "xpath points to non-object" },
{Error::XpathIsNull, "xpath argument is null pointer" },
{Error::MissingBitPosInfoForOptField, "no presnc-mapping information for optional field" },
{Error::JsonFieldTypeMismatch, "json field value type differs from C counterpart" },
//{Error::RequiredFieldIsNullType, "required field value has null type" },
{Error::MissingOptionalValuesTable, "missing optional values table (mapping for presnc bits)" },
{ Error::AtUnreachable, "$#%^$%^%$" },
{Error::MissingRequiredField, "missing required field" },
{ Error::InappropriatePresncType, "internal error: presnc field has wrong type" },

{ Error::ArrayDbl3x3_IsNotArray, "ArrayDbl3x3_IsNotArray" },
{ Error::ArrayDbl3x3_Size_is_not_3, "ArrayDbl3x3_Size_is_not_3" },

{ Error::ArrayDbl1x3_IsNotArray, "ArrayDbl1x3_IsNotArray" },
{ Error::ArrayDbl1x3_Size_is_not_3, "ArrayDbl1x3_Size_gt_3" },
{ Error::ArrayDbl1x3_ElementIsNotNumber, "ArrayDbl1x3_InappropriateElementType" },

{ Error::ArrayChar1x3_IsNotString, "ArrayChar1x3_IsNotString" },
{ Error::ArrayChar1x3_Size_gt_3, "ArrayChar1x3_Size_gt_3" },

{ Error::ETblDbl_IsNotArray, "ETblDbl_IsNotArray" },
{ Error::ETblDbl_IsEmpty, "ETblDbl_IsEmpty" },
{ Error::ETblDbl_SubArrayIsNotArray, "ETblDbl_SubArrayIsNotArray" },
{ Error::ETblDbl_SubArrayIsEmpty, "ETblDbl_SubArrayIsEmpty" },
{ Error::ETblDbl_LeafElementIsNotNumber, "ETblDbl_LeafElementIsNotNumber" },
{ Error::ETblDbl_SubArraySizeMismatch, "ETblDbl_SubArraySizeMismatch" },

{ Error::ER3ADbl_IsNotArray, "ER3ADbl_IsNotArray" },
{ Error::ER3ADbl_IsEmpty, "ER3ADbl_IsEmpty" },
{ Error::ER3ADbl_SubArray_IsNotArray, "ER3ADbl_SubArray_IsNotArray" },
{ Error::ER3ADbl_SubArray_IsEmpty, "ER3ADbl_SubArray_IsEmpty" },
{ Error::ER3ADbl_SubSubArray_IsNotArray, "ER3ADbl_SubSubArray_IsNotArray" },
{ Error::ER3ADbl_SubSubArray_IsEmpty, "ER3ADbl_SubSubArray_IsEmpty" },
{ Error::ER3ADbl_LeafElementIsNotNumber, "ER3ADbl_LeafElementIsNotNumber" },
{ Error::ER3ADbl_SubArraySizeMismatch, "ER3ADbl_SubArraySizeMismatch" },
{ Error::ER3ADbl_SubSubArraySizeMismatch, "ER3ADbl_SubSubArraySizeMismatch" },

{ Error::mean_profile_drag_IsNotObject, "mean_profile_drag_IsNotObject" },
{ Error::mean_profile_drag_Missing_CombiTableDbl_Field, "mean_profile_drag_Missing_CombiTableDbl_Field" },
{ Error::mean_profile_drag_CombiTableDbl_IsNull, "mean_profile_drag_CombiTableDbl_IsNull" },
{ Error::mean_profile_drag_CombiTableDbl_IsNotObject, "mean_profile_drag_CombiTableDbl_IsNotObject" },

{ Error::EArrayDbl_InappropriateElementType, "EArrayDbl_InappropriateElementType" },
{ Error::process_number_unexpected_number_type, "process_number_unexpected_number_type" },

{ Error::init_parser_from_file_null_this, "init_parser_from_file:  this argument is null pointer" },
{ Error::init_parser_from_string_null_this, "init_parser_from_string:  this argument is null pointer" },
{ Error::set_alloc_null_this, "set_alloc:  this argument is null pointer" },
{ Error::fini_parser_null_this, "fini_parser:  this argument is null pointer" },
{ Error::parse_object_null_this, "parse_object:  this argument is null pointer" },
{ Error::parse_object_null_xpath, "parse_object:  xpath argument is null pointer" },
{ Error::parse_object_null_output_buffer, "parse_object:  output buffer argument is null pointer" },
{ Error::get_error_string_null_this, "get_error_string: this argument is null pointer" },
{ Error::get_error_string_null_strptr, "get_error_string: output object argument is null pointer" },
{Error::wrong_error_id , "wrong_error_id"}
};

//////////////////////////////////////////////////
struct Jdp
{
    FILE* fp;
    FileReadStream *frs;
    Document *doc;
    char readBuffer[0x10000];

    bool json_top_layout_seems_ok;
    fn_malloc_t fn_malloc;

    bool is_test_mode;

    rapidjson::Value errorCause;
    std::string currentField;

    Error::Code AllocStringData(const char *src, uint32_t len, uint8_t* &p_strdata)
    {
	p_strdata = nullptr;

    	if (!fn_malloc)
	    return Error::Malloc_function_not_assigned;

	auto len_z = len + 1;

    	auto p = static_cast<uint8_t*>(fn_malloc(len_z));
	if (p == nullptr)
	    return Error::Malloc_failed;

	memcpy(p, src, len);
	p[len] = 0;

	p_strdata = p;

	return Error::NoError;
    }

    Error::Code AllocDoublesAndInitWithDefValue(uint32_t element_count, double* &p_doubles)
    {
	p_doubles = nullptr;

	if (!fn_malloc)
	    return Error::Malloc_function_not_assigned;

	auto bytes_count = element_count * sizeof(double);
	auto p = static_cast<double*>(fn_malloc(bytes_count));
	if (p == nullptr)
	    return Error::Malloc_failed;

	memset(p, 0, bytes_count);

	p_doubles = p;

	return Error::NoError;
    }

    ///////////////////////////////////////////////////////////////
    
	Jdp(bool is_test_mode=false) : fp(nullptr), frs(nullptr), doc(nullptr), fn_malloc(nullptr), is_test_mode(is_test_mode) {}
    ~Jdp()
    {
	Reset();
    }

    void Reset()
    {
	delete doc;
	doc = nullptr;

	delete frs;
	frs = nullptr;

	if (fp != nullptr)
	    fclose(fp);
	fp = nullptr;

	json_top_layout_seems_ok = false;

    }

    // проверить состояние парсера
	Error::Code CheckState()
    {
	if (!fn_malloc)
	    return Error::Malloc_function_not_assigned;
	if (!doc)
	    return Error::DocumentIsNull;
	if(doc->HasParseError())
	    return Error::DocumentParseError;
	if (!json_top_layout_seems_ok)
	    return Error::JsonTopLayoutMismatch;

	return Error::NoError;
    }

    Error::Code SimpleCheckOfTopLayout()
    {
	if (!doc)
	    return Error::DocumentIsNull;
	// in test mode root object can be any type
	if (!is_test_mode)
	{
	    if (!doc->IsObject())
		return Error::DocumentIsNotObject;

	    auto version = GetValueByPointer(*doc, "/version");
	    // TODO: add trim to version string before compare
	    if (!version)
		return Error::MissingVersion;
	    if (!version->IsString())
		return Error::VersionIsNotString;
	    if (strcmp(version->GetString(), "0.1.8"))
		return Error::VersionMismatch;

	    auto _units_ = GetValueByPointer(*doc, "/_units_");
	    if (!_units_)
		return Error::MissingUnitsObject;
	    if (!_units_->IsObject())
		return Error::UnitsIsNotObject;

	    auto _data_ = GetValueByPointer(*doc, "/_data_");
	    if (!_data_)
		return Error::MissingDataObject;
	    if (!_data_->IsObject())
		return Error::DataIsNotObject;
	}
	json_top_layout_seems_ok = true;
	return Error::NoError;
    }


    Error::Code ParseFile(const char* filename)
    {
	Reset();

	fp = fopen(filename, "rb");
	if (fp == nullptr)
	    return Error::FileOpenError;

	frs = new FileReadStream(fp, readBuffer, sizeof(readBuffer));
	doc = new Document();
	doc->ParseStream(*frs);
	if (doc->HasParseError())
	{
	    return Error::DocumentParseError;
	}
	return SimpleCheckOfTopLayout();
    }

    Error::Code ParseText(const char* text)
    {
	Reset();

	//if (fp != nullptr || frs != nullptr)
	//    return Error::Err;
	doc = new Document();
	doc->Parse(text);
	if (doc->HasParseError())
	{
	    return Error::DocumentParseError;
	}
	return SimpleCheckOfTopLayout();
    }

    Error::Code set_alloc(fn_malloc_t fn)
    {
	fn_malloc = fn;
	return Error::NoError;
    }

    Error::Code parse_xpath_object(objinfo_t *p_oi, const char* xpath, void*buf)
	{

	JUST_DO_IT(CheckState()) ;

	auto value = Pointer(xpath).Get(*doc);
	if (!value)
	    return Error::XpathPointsToNothing;

	if (value->IsNull())
	{
	    // default value must be inited by memset(x, 0, y)
	    return Error::NoError;
	}

	if (!value->IsObject())
	    return Error::XpathValueIsNotObject;

	auto o = value->GetObject();
	auto p_cb = static_cast<uint8_t*>(buf);

	return process_object(o, p_oi, p_cb);

    }

    Error::Code get_error_string(Error::Code err, EString* str)
    {
	if (!str)
	    return Error::get_error_string_null_strptr;
	if (!fn_malloc)
	    return Error::Malloc_function_not_assigned;
	if (!error_strings.count(err))
	    return Error::wrong_error_id;

	auto s = error_strings[err];
	auto slen = strlen(s);
	auto dst = static_cast<char*>(fn_malloc(slen + 1));
	if (!dst)
	    return Error::Malloc_failed;

	memcpy(dst, s, slen);
	dst[slen] = 0;

	str->bytes = slen;
	str->len = utf8len(s, slen);
	str->text = dst;

	return Error::NoError;
    }

    //////////////////////////////////////////////////////////////
    bool match(JsonType::Enum expected, rapidjson::Type valType)
    {
	switch (valType)
	{
	case kNullType: return expected == JsonType::Null;
	case kFalseType: return expected == JsonType::False;
	case kTrueType: return expected == JsonType::True;
	case kObjectType: return expected == JsonType::Object;
	case kArrayType: return expected == JsonType::Array;
	case kStringType: return expected == JsonType::String;
	case kNumberType: return expected >= JsonType::Number;
	default:
	    return false;
	}
    }

    Error::Code get_presnc_bit(optvalue_t *ovt, JsonKeyHashes hash, uint64_t *bitval)
    {
	auto p = ovt;
	    while(p->hash != noname_hash)
	    {
		    if(p->hash == hash)
		    {
			*bitval = uint64_t(1) << p->bitpos;
			return Error::NoError;
		    }
		    p++;
	    }
	    return Error::MissingBitPosInfoForOptField;
    }

    Error::Code init_null_object(objinfo_t *objinfo, uint8_t* co)
    {
	field_t *presnc_fi = nullptr;

	for (unsigned i = 0, n = objinfo->field_count; i < n; i++) {
	    auto &fld = objinfo->fields[i];
	    auto type = unpack_type(fld.otf);
	    if(type == JsonType::Object)
	    {
		init_null_object(static_cast<objinfo_t *>(fld.handler), co + unpack_offset(fld.otf));
	    }
	    if(fld.name == presnc_hash)
	    {
		presnc_fi = &fld;
	    }
	}

	if (!presnc_fi)
	    return Error::NoError;

	auto ovt = static_cast<optvalue_t*>(presnc_fi->handler);
	auto p = ovt;
	uint64_t presnc = 0;
	while (p->hash != noname_hash)
	{
	    presnc |= uint64_t(1) << p->bitpos;
	    p++;
	}

	auto presnc_field_data_offset = co + unpack_offset(presnc_fi->otf);
	return set_presnc_field(presnc, presnc_field_data_offset, unpack_type(presnc_fi->otf));
    }

    Error::Code set_presnc_field(uint64_t presnc, uint8_t *field_data_offset, JsonType::Enum fld_type)
    {
	switch (fld_type)
	{
	case JsonType::Uint8:
	    *field_data_offset = uint8_t(presnc);
	    break;
	case JsonType::Uint16:
	    *reinterpret_cast<uint16_t*>(field_data_offset) = uint16_t(presnc);
	    break;
	case JsonType::Uint32:
	    *reinterpret_cast<uint32_t*>(field_data_offset) = uint32_t(presnc);
	    break;
	case JsonType::UInt64:
	    *reinterpret_cast<uint64_t*>(field_data_offset) = uint64_t(presnc);
	    break;
	default:
	    return  Error::InappropriatePresncType;
	}
	return Error::NoError;
    }

    Error::Code process_object(Value::Object o, objinfo_t *objinfo, uint8_t *co) {
    //std::unordered_set<JsonKeyHashes> missing_fields;
    std::unordered_set<uint32_t> missing_fields;
	objinfo->get_field_names(missing_fields);

	uint64_t presnc = 0;
	optvalue_t *ovt = nullptr;
	const auto presnc_fi = objinfo->Find(presnc_hash);
	if(presnc_fi)
	{
	    ovt = static_cast<optvalue_t*>(presnc_fi->handler);
	}

	for (auto it = o.MemberBegin(), e = o.MemberEnd(); it != e; ++it)
	{
	    auto key = it->name.GetString();
	    auto keylen = it->name.GetStringLength();

	    auto hash = static_cast<JsonKeyHashes>(MurmurHash3_x86_32(key, keylen, mm3_seed));

	    // требуется ли для данного поля десериализация? 
	    auto fi = objinfo->Find(hash);
	    if (fi == nullptr)
		continue;

	    // удалить название данного поля из списка отсутствующих
	    missing_fields.erase(hash);

	    auto expected_type = unpack_type(fi->otf);
	    auto val_type = it->value.GetType();
	    if (!match(expected_type, val_type) && val_type != kNullType)
	    {
		return Error::JsonFieldTypeMismatch;
	    }

	    if (flags_is_opt_field(fi->otf))
	    {
		if (ovt == nullptr)
		    return Error::MissingOptionalValuesTable;

		uint64_t flag = 0;
		JUST_DO_IT(get_presnc_bit(ovt, hash, &flag));
		presnc |= flag;
	    }

	    auto field_data_offset = co + unpack_offset(fi->otf);
	    auto ptr = fi->handler;

	    // DIRTY HACK !!!
	    if (hash == MeanProfileDrag_hash)
	    {
		// if MeanProfileDrag == null continue
		// if MeanProfileDrag.CombiTableDbl == null continue
		// if MeanProfileDrag missing continue
		// if MeanProfileDrag.CombiTableDbl missing continue
		// else goto spec_handler 

		if (val_type != kNullType)
		{
			auto mpd = it->value.GetObject();
			if (mpd.HasMember("CombiTableDbl"))
			{
				auto& vv = mpd["CombiTableDbl"];
				if (vv.IsNull())
				{
					// do nothing
				    ;
				} else if (vv.IsObject())
				{
				    JUST_DO_IT(process_object(vv.GetObject(), &CombiTableDbl_obj_info, (uint8_t*)field_data_offset));
				} else
				{
				    return Error::JsonFieldTypeMismatch;
				}
			} else
			{
			    // missing optional field
			    // do nothing
			}
		}
	    	continue;
	    }

	    if (val_type == kNullType)
	    {

		if (expected_type == JsonType::Object)
		{
		    init_null_object(static_cast<objinfo_t*>(ptr), field_data_offset);
		}

		// null тип означает что, данное поле структуры имеет значение по умолчанию
		// default value must be preset by memset
		continue;
	    }
	    


	    if(hash == _ref_units__hash)
	    {
		// ignore
		if (is_test_mode)
		    continue;

		auto xpath = it->value.GetString();
		auto slen = it->value.GetStringLength();
		if (xpath[0] == '#')
		{
		    // skip #
		    xpath++;
		    slen--;
		}
		std::string tmp(xpath, slen);
		JUST_DO_IT(parse_xpath_object(static_cast<objinfo_t*>(ptr), tmp.c_str(), field_data_offset));
		continue;
	    }

	    if (flags_has_spec_handler(fi->otf))
	    {
		auto p_func = reinterpret_cast<field_handler_t>(ptr);
		JUST_DO_IT(p_func(this, it->value, field_data_offset));
	    }
	    else
	    {
		switch (val_type)
		{
		case kObjectType:
		{
		    JUST_DO_IT(process_object(it->value.GetObject(), static_cast<objinfo_t*>(ptr), field_data_offset));
		    break;
		}
		case kArrayType:
		{
		    JUST_DO_IT(process_EArrayDbl(it->value, field_data_offset));
		    break;
		}
		case kStringType:
		    JUST_DO_IT(process_string(it->value, field_data_offset));
		break;
		case kNumberType:
		    JUST_DO_IT(process_number(it->value, expected_type, field_data_offset));
		    break;

		case kNullType:
		    // something went wrong
		    return Error::AtUnreachable;

		case kFalseType:
		case kTrueType:
		default:
		    return Error::JsonFieldTypeMismatch;
		}
	    }
	}

	// проверка пропущенных в json объекте полей С объенкта
    	for (auto h : missing_fields)
	{
        auto fi = objinfo->Find(JsonKeyHashes(h));
	    if (!fi)
	    {
		// недостижимая ветка, так как по условию данные хеши получены через обход этого самого objinfo
		return Error::AtUnreachable;
	    }
	    if (!flags_is_opt_field(fi->otf))
		return Error::MissingRequiredField;
	}

	// записать presnc в поле С объекта 
	if(presnc_fi)
	{
	    auto field_data_offset = co + unpack_offset(presnc_fi->otf);
	    JUST_DO_IT(set_presnc_field(presnc, field_data_offset, unpack_type(presnc_fi->otf)));
	}

	return Error::NoError;
    }

    ////////////////////////////////////////
    Error::Code process_array_dbl_3x3(Value &v, void* outp)
    {
	auto p = static_cast<double*>(outp);

	if(v.IsNull())
	{
	    static_assert(sizeof(T_inertia) >= sizeof(double) * 3 * 3,"");
	    memset(p, 0, sizeof(T_inertia));

	    return Error::NoError;
	}

	if(!v.IsArray())
	    return Error::ArrayDbl3x3_IsNotArray;

	auto a = v.GetArray();
	if (a.Size() != 3)
	    return Error::ArrayDbl3x3_Size_is_not_3;

	for(unsigned i=0; i <3;i++)
	{
	    JUST_DO_IT(process_array_dbl_1x3(a[i], p));
	    p += 3;
	}
	return Error::NoError;
    }

     Error::Code process_array_dbl_1x3(Value &v, void* outp)
    {
	 auto p = static_cast<double*>(outp);
	if(v.IsNull())
	{
	    memset(p, 0, sizeof(double) * 3);
	    return Error::NoError;
	}

	if (!v.IsArray())
	    return Error::ArrayDbl1x3_IsNotArray;

	auto a = v.GetArray();
	if(a.Size() != 3)
	    return Error::ArrayDbl1x3_Size_is_not_3;

	for (unsigned i = 0; i < 3; i++, p++)
	{
	    if (a[i].IsNumber())
	    {
		*p = a[i].GetDouble();
	    } else if(a[i].IsNull())
	    {
		*p = 0;
	    }
	    else
	    {
		// unexpected data type
		return Error::ArrayDbl1x3_ElementIsNotNumber;
	    }
	}
	return Error::NoError;
    }
    Error::Code process_array_char_1x3(Value &v, void* outp)
    {
	auto p = static_cast<char*>(outp);

	if(v.IsNull())
	{
	    p[0] = p[1] = p[2] = 0;
	    return Error::NoError;
	}

	if(!v.IsString())
	    return Error::ArrayChar1x3_IsNotString;

	auto slen = v.GetStringLength();
	if(slen > 3)
	    return Error::ArrayChar1x3_Size_gt_3;

	p[0] = p[1] = p[2] = 0;
	auto s = v.GetString();
	for(unsigned i=0; i<slen; i++, p++)
	{
	    *p = s[i];
	}
	return Error::NoError;
    }
    Error::Code determine_table_column_count(Value::Array &a,uint32_t &dim)
    {
	dim = 0;
	bool has_nonnull_sa = false;
	for(unsigned j=0, n= a.Size(); j < n; j++)
	{
	    auto &v = a[j];
	    if (v.IsNull())
		continue;
	    if (!v.IsArray())
		return Error::ETblDbl_SubArrayIsNotArray;
	    auto sa = v.GetArray();
	    if(!has_nonnull_sa)
	    {
		has_nonnull_sa = true;
		dim = sa.Size();
	    } else if(sa.Size() != dim)
		return Error::ETblDbl_SubArraySizeMismatch;
	}
	if(!has_nonnull_sa)
	{
	    return Error::ETblDbl_AllSubArraysAreNullObjects;
	}
	
	return Error::NoError;
    }

    Error::Code copy_doubles(Value::Array &sa, double* &p)
    {
	for (unsigned j = 0, n = sa.Size(); j < n; j++, p++)
	{
	    auto &v = sa[j];
	    if (v.IsNumber())
	    {
		*p = v.GetDouble();
	    }
	    else if (!v.IsNull())
	    {
		return Error::ETblDbl_LeafElementIsNotNumber;
	    }
	}
	return Error::NoError;
    }

    Error::Code copy_copy_doubles(Value::Array &a, unsigned column_count, double*p)
    {
	for (unsigned i = 0, n = a.Size(); i<n; i++)
	{
	    if (a[i].IsNull())
	    {
		// already inited with def values
		p += column_count;
	    }
	    else
	    {
		auto sa = a[i].GetArray();
		JUST_DO_IT(copy_doubles(sa, p));
	    }
	}
	return Error::NoError;
    }

    Error::Code process_ETblDbl(Value &v, void* outp)
    {
	auto et = static_cast<ETableDbl*>(outp);

	if(v.IsNull())
	{
	    et->columns = et->rows = 0;
	    et->table = nullptr;
	    return Error::NoError;
	}

	if (!v.IsArray())
	    return Error::ETblDbl_IsNotArray;

	auto a = v.GetArray();
	auto row_count = a.Size();
	if(row_count < 1)
	{
	    et->columns = et->rows = 0;
	    et->table = nullptr;
	    return Error::NoError;
	}

	uint32_t column_count = 0;
	JUST_DO_IT(determine_table_column_count(a, column_count));

	if(column_count == 0)
	{
	    // weird case
	    et->rows = row_count;
	    et->columns = 0;
	    et->table = nullptr;
	    return Error::NoError;
	}

	double *p_doubles;
	JUST_DO_IT(AllocDoublesAndInitWithDefValue(row_count * column_count, p_doubles));

    	et->rows = row_count;
	et->columns = column_count;
	et->table = p_doubles;

	JUST_DO_IT(copy_copy_doubles(a, column_count, p_doubles));

	return Error::NoError;
    }

    Error::Code erank3_array_dbl_determine_table_dimensions(Value::Array &a, uint32_t &dimX, uint32_t &dimY)
    {
	dimX = dimY = 0;
	bool has_nonnull_sa = false;
	bool has_nonnull_ssa = false;
	for (unsigned i = 0, n = a.Size(); i < n; i++)
	{
	    auto &v = a[i];
	    if (v.IsNull())
		continue;
	    if (!v.IsArray())
		return Error::ER3ADbl_SubArray_IsNotArray;
	    auto sa = v.GetArray();
	    if (!has_nonnull_sa)
	    {
		has_nonnull_sa = true;
		dimX = sa.Size();
	    }
	    else if (sa.Size() != dimX)
		return Error::ER3ADbl_SubArraySizeMismatch;

	    for (unsigned j = 0; j < dimX; j++)
	    {
		auto &vv = sa[j];
		if (vv.IsNull())
		    continue;
		if (!vv.IsArray())
		    return Error::ER3ADbl_SubSubArray_IsNotArray;
		auto ssa = vv.GetArray();
		if (!has_nonnull_ssa)
		{
		    has_nonnull_ssa = true;
		    dimY = ssa.Size();
		}
		else if (ssa.Size() != dimY)
		    return Error::ER3ADbl_SubSubArraySizeMismatch;
	    }
	}
	if (!has_nonnull_sa)
	{
	    return Error::ER3ADbl_AllSubArraysAreNullObjects;
	}
    	if(!has_nonnull_ssa)
	{
	    return Error::ER3ADbl_AllSubSubArraysAreNullObjects;
	}
	return Error::NoError;
    }

    /*
     [ [ [1,2,3], [4,5,6], [7,8,9], null, [null, null, null] ], null, [ ... ] ]
    */
    Error::Code process_ERank3ArrayDbl(Value &v, void* outp)
    {
	auto r3 = static_cast<ERank3ArrayDbl*>(outp);

	// null case
	if(v.IsNull())
	{
	    r3->rows = r3->columns = r3->pages = 0;
	    r3->table = nullptr;
	    return Error::NoError;
	}

	if (!v.IsArray())
	    return Error::ER3ADbl_IsNotArray;

	auto a = v.GetArray();
	auto x_count = a.Size();

	// [] case
    	if (x_count < 1)
	{
	    r3->rows = r3->columns = r3->pages = 0;
	    r3->table = nullptr;
	    return Error::NoError;
	}

	uint32_t y_count=0, z_count=0;
	JUST_DO_IT(erank3_array_dbl_determine_table_dimensions(a,  y_count, z_count));

	double *p_doubles;
	JUST_DO_IT(AllocDoublesAndInitWithDefValue(x_count * y_count * z_count, p_doubles));

	r3->rows = x_count;
	r3->columns = y_count;
	r3->pages = z_count;
	r3->table = p_doubles;

	auto p = p_doubles;
	for (unsigned i = 0; i<x_count; i++)
	{
	    auto &v = a[i];
	    if (v.IsNull())
	    {
		p += sizeof(double) * y_count * z_count;
		continue;
	    }

	    auto sa = v.GetArray();
	    for (unsigned j = 0; j<y_count; j++)
	    {
		auto &vv = sa[j];
		if (vv.IsNull())
		{
		    p += sizeof(double) * z_count;
		    continue;
		}

		auto ssa = vv.GetArray();
		for (unsigned k = 0; k<z_count; k++, p++)
		{
		    auto &vvv = ssa[k];

		    if (vvv.IsNumber())
		    {
			*p = vvv.GetDouble();
		    } else if (!vvv.IsNull())
		    {
			return Error::ER3ADbl_LeafElementIsNotNumber;
		    }
		}
	    }

	}


	return Error::NoError;
    }

    Error::Code process_EArrayDbl(Value &v, void* field_data_offset)
    {
	auto arr = static_cast<EArrayDbl*>(field_data_offset);


	if(v.IsNull())
	{
		arr->n = 0;
		arr->values = nullptr;
		return Error::NoError;
	}

	if(!v.IsArray())
	{
	    return Error::JsonFieldTypeMismatch;
	}

	auto a = v.GetArray();
	if(a.Size() < 1)
	{
	    arr->n = 0;
	    arr->values = nullptr;
	    return Error::NoError;
	}

	double *p;
	
    	JUST_DO_IT(AllocDoublesAndInitWithDefValue(a.Size(), p));

	arr->n = a.Size();
	arr->values = p;

	for (auto it = a.Begin(), e = a.End(); it != e; ++it, ++p)
	{
	    if (it->IsNumber())
	    {
		*p = it->GetDouble();
	    }
	    else if (it->IsNull()) {
		*p = 0;
	    }
	    else
	    {
		return Error::EArrayDbl_InappropriateElementType;
	    }
	}
	return Error::NoError;
    }

    Error::Code process_number(Value &v, JsonType::Enum type, void* field_data_offset)
    {
	//if (!v.IsNumber())
	//    return Error::Err;
	switch(type)
	{
	case JsonType::Uint8:
	    *static_cast<uint8_t*>(field_data_offset) = uint8_t(v.GetUint());
	    break;
	case JsonType::Uint16:
	    *static_cast<uint16_t*>(field_data_offset) = uint16_t(v.GetUint());
	    break;
	case JsonType::Uint32:
	    *static_cast<uint32_t*>(field_data_offset) = v.GetUint();
	    break;
	case JsonType::UInt64:
	    *static_cast<uint64_t*>(field_data_offset) = v.GetUint64();
	    break;
	case JsonType::Float:
	    *static_cast<float*>(field_data_offset) = v.GetFloat();
		break;

	case JsonType::Double:
	    *static_cast<double*>(field_data_offset) = v.GetDouble();
		break;
	default:
	    return Error::process_number_unexpected_number_type;
	}
	return Error::NoError;
    }

    Error::Code process_string(Value &v, void* outp)
    {
	//if (!v.IsString())
	//    return Error::Err;
	auto src = v.GetString();
	auto slen = v.GetStringLength();
	uint8_t*dst;
	JUST_DO_IT(AllocStringData(src, slen, dst));
	auto es = static_cast<EString *>(outp);
	es->text = reinterpret_cast<char*>(dst);
	es->bytes = slen;
    	es->len = utf8len(src, slen);

    	return Error::NoError;
    }

};

////////////////////////////////////////
Error::Code fixed_array_dbl_3x3_(Jdp *_this, Value &a, void*objinfo)
{
    return _this->process_array_dbl_3x3(a, objinfo);
}
Error::Code fixed_array_dbl_1x3_(Jdp *_this, Value &a, void*objinfo)
{
    return _this->process_array_dbl_1x3(a, objinfo);
}
Error::Code fixed_array_char_1x3_(Jdp *_this, Value &a, void*objinfo)
{
    return _this->process_array_char_1x3(a, objinfo);
}
Error::Code ETblDbl_handler_(Jdp *_this, Value &a, void*objinfo)
{
    return _this->process_ETblDbl(a, objinfo);
}
Error::Code ER3ADbl_handler_(Jdp *_this, Value &a, void*objinfo)
{
    return _this->process_ERank3ArrayDbl(a, objinfo);
}

////////////////////////////////////////

#ifdef __cplusplus
extern "C" {
#endif

    JDPSHARED_EXPORT Error::Code init_parser_from_file(const char* fname, void**_this)
{
    auto jdp = new Jdp();
    *_this = jdp;

    if (jdp == nullptr)
	return Error::init_parser_from_file_null_this;

    return jdp->ParseFile(fname);
}
    JDPSHARED_EXPORT Error::Code init_parser_from_string(const char* json_text, void**_this)
{
    auto jdp = new Jdp();
    *_this = jdp;

    if (jdp == nullptr)
	return Error::init_parser_from_string_null_this;

    return jdp->ParseText(json_text);
}

    JDPSHARED_EXPORT Error::Code set_alloc(void* _this, fn_malloc_t fn)
    {
	auto p = static_cast<Jdp*>(_this);
	if (p == nullptr)
	    return Error::set_alloc_null_this;
	return p->set_alloc(fn);
    }

    JDPSHARED_EXPORT Error::Code fini_parser(void* _this)
{
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::fini_parser_null_this;
    delete o;
    return Error::NoError;
}

    JDPSHARED_EXPORT Error::Code parse_object(void* _this, TypeIds::Enum type_id, const char* xpath, void* buf)
{
	if (!buf)
	    return Error::parse_object_null_output_buffer;

	if (!tiso.count(type_id))
	    return Error::TypeIdIsNotMapped;

	auto pv = tiso[type_id];
	auto p_oi = pv.first;
	auto objlen = pv.second;

	memset(buf, 0, objlen);

	if (!_this)
	    return Error::parse_object_null_this;
	if(!xpath)
	    return Error::parse_object_null_xpath;

	return static_cast<Jdp*>(_this)->parse_xpath_object(p_oi, xpath, buf);

}

    JDPSHARED_EXPORT Error::Code get_error_string(void* _this, Error::Code err, EString* str)
{
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::get_error_string_null_this;
	return o->get_error_string(err, str);

}

    ////////////////////////////////////////
    JDPSHARED_EXPORT Error::Code test_init_parser_from_string(const char* json_text, void**_this)
    {
	auto jdp = new Jdp(true);
	*_this = jdp;

	if (jdp == nullptr)
	    return Error::init_parser_from_string_null_this;

	return jdp->ParseText(json_text);
    }

    JDPSHARED_EXPORT Error::Code test_parse_string(void* _this, EString* co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(EString));
    	return o->process_string(*o->doc, co);
    }

    JDPSHARED_EXPORT Error::Code test_parse_1x3_dbl_array(void* _this, void* co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(double) * 3);
	return o->process_array_dbl_1x3(*o->doc, co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_3x3_dbl_array(void* _this, void* co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(double) * 3 * 3 );
	return o->process_array_dbl_3x3(*o->doc, co);
    }

    JDPSHARED_EXPORT Error::Code test_parse_1x3_char_array(void* _this, void* co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, 3);
	return o->process_array_char_1x3(*o->doc, co);
    }

    JDPSHARED_EXPORT Error::Code test_parse_EArrayDbl(void* _this, EArrayDbl *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(EArrayDbl));
	return o->process_EArrayDbl(*o->doc, co);
    }

    JDPSHARED_EXPORT Error::Code test_parse_ETableDbl(void* _this, ETableDbl *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(ETableDbl));
	return o->process_ETblDbl(*o->doc, co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_ERank3ArrayDbl(void* _this, ERank3ArrayDbl *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(ERank3ArrayDbl));
	return o->process_ERank3ArrayDbl(*o->doc, co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_EPoly2Dbl(void* _this, EPoly2Dbl *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(EPoly2Dbl));
	return o->process_object(o->doc->GetObject(), &EPoly2Dbl_obj_info, (uint8_t*)co);
    }

    JDPSHARED_EXPORT Error::Code test_parse_UnitsInertia(void* _this, UnitsInertia *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(UnitsInertia));
	return o->process_object(o->doc->GetObject(), &UnitsInertia_obj_info, (uint8_t*)co);
    }

    JDPSHARED_EXPORT Error::Code test_parse_UnitsCombiTable(void* _this, UnitsCombiTable *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(UnitsCombiTable));
	return o->process_object(o->doc->GetObject(), &UnitsCT_o_i, (uint8_t*)co);
    }

    JDPSHARED_EXPORT Error::Code test_parse_UnitsCombi3Table(void* _this, UnitsCombi3Table *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(UnitsCombi3Table));
	return o->process_object(o->doc->GetObject(), &UnitsC3T_o_i, (uint8_t*)co);
    }

    JDPSHARED_EXPORT Error::Code test_parse_CombiTableDbl(void* _this, CombiTableDbl *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(CombiTableDbl));
	return o->process_object(o->doc->GetObject(), &CombiTableDbl_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_Combi3TableDbl(void* _this, Combi3TableDbl *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(Combi3TableDbl));
	return o->process_object(o->doc->GetObject(), &Combi3TableDbl_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_UnitsPosition(void* _this, UnitsPosition *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(UnitsPosition));
	return o->process_object(o->doc->GetObject(), &UnitsPosition_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_T_position(void* _this, T_position *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(T_position));
	return o->process_object(o->doc->GetObject(), &T_position_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_T_factor(void* _this, T_factor *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(T_factor));
	return o->process_object(o->doc->GetObject(), &T_factor_obj_info, (uint8_t*)co);
    }

    JDPSHARED_EXPORT Error::Code test_parse_T_VectorCombiTableDbl(void* _this, T_VectorCombiTableDbl *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(T_VectorCombiTableDbl));
	return o->process_object(o->doc->GetObject(), &T_VectorCTDbl_obj_info, (uint8_t*)co);
    }

    JDPSHARED_EXPORT Error::Code test_parse_UnitsTwist(void* _this, UnitsTwist *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(UnitsTwist));
	return o->process_object(o->doc->GetObject(), &UnitsTwist_obj_info, (uint8_t*)co);
    }

    JDPSHARED_EXPORT Error::Code test_parse_UnitsBladeSimple(void* _this, UnitsBladeSimple *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(UnitsBladeSimple));
	return o->process_object(o->doc->GetObject(), &UnitsBladeSimple_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_T_param_blade_simple(void* _this, T_param_blade_simple *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(T_param_blade_simple));
	return o->process_object(o->doc->GetObject(), &T_param_blade_simple_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_T_param_enginewithregulator(void* _this, T_param_enginewithregulator *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(T_param_enginewithregulator));
	return o->process_object(o->doc->GetObject(), &T_param_enginewithregulator_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_UnitsTransmission1M1T(void* _this, UnitsTransmission1M1T *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(UnitsTransmission1M1T));
	return o->process_object(o->doc->GetObject(), &UnitsTransmiss_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_T_param_transmission_1m1t_rotor(void* _this, T_param_transmission_1m1t_rotor *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(T_param_transmission_1m1t_rotor));
	return o->process_object(o->doc->GetObject(), &T_param_transmission_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_UnitsHingeGeneral(void* _this, UnitsHingeGeneral *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(UnitsHingeGeneral));
	return o->process_object(o->doc->GetObject(), &UnitsHingeGeneral_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_T_param_hinge_general(void* _this, T_param_hinge_general *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(T_param_hinge_general));
	return o->process_object(o->doc->GetObject(), &T_param_hinge_general_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_T_param_hub_simple(void* _this, T_param_hub_simple *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(T_param_hub_simple));
	return o->process_object(o->doc->GetObject(), &T_param_hub_simple_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_UnitsSwashplate(void* _this, UnitsSwashplate *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(UnitsSwashplate));
	return o->process_object(o->doc->GetObject(), &UnitsSwashplate_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_T_param_swashplate(void* _this, T_param_swashplate *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(T_param_swashplate));
	return o->process_object(o->doc->GetObject(), &T_param_swashplate_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_T_param_cet_1m1t_rotor(void* _this, T_param_cet_1m1t_rotor *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(T_param_cet_1m1t_rotor));
	return o->process_object(o->doc->GetObject(), &T_param_cet_1m1t_rotor_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_UnitsRotorSimple(void* _this, UnitsRotorSimple *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(UnitsRotorSimple));
	return o->process_object(o->doc->GetObject(), &UnitsRotorSimple_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_T_param_rotor_simple(void* _this, T_param_rotor_simple *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(T_param_rotor_simple));
	return o->process_object(o->doc->GetObject(), &T_param_rotor_simple_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_UnitsHTail(void* _this, UnitsHTail *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(UnitsHTail));
	return o->process_object(o->doc->GetObject(), &UnitsHTail_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_T_param_H_tail(void* _this, T_param_H_tail *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(T_param_H_tail));
	return o->process_object(o->doc->GetObject(), &T_param_H_tail_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_UnitsVTail(void* _this, UnitsVTail *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(UnitsVTail));
	return o->process_object(o->doc->GetObject(), &UnitsVTail_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_T_param_V_tail(void* _this, T_param_V_tail *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(T_param_V_tail));
	return o->process_object(o->doc->GetObject(), &T_param_V_tail_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_UnitsFuselageSimple(void* _this, UnitsFuselageSimple *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(UnitsFuselageSimple));
	return o->process_object(o->doc->GetObject(), &UnitsFuselageSimple_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_T_param_fuselage_simple(void* _this, T_param_fuselage_simple *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(T_param_fuselage_simple));
	return o->process_object(o->doc->GetObject(), &T_param_fuselage_obj_info, (uint8_t*)co);
    }
    JDPSHARED_EXPORT Error::Code test_parse_T_param_simple_one_rotor_helicopter(void* _this, T_param_simple_one_rotor_helicopter *co)
    {
	auto o = static_cast<Jdp*>(_this);
	if (o == nullptr)
	    return Error::null_this;
	memset(co, 0, sizeof(T_param_simple_one_rotor_helicopter));
	return o->process_object(o->doc->GetObject(), &T_param_simple_one_rotor_helicopter_obj_info, (uint8_t*)co);
    }

/*
 *1. _data_/Body/AeroForceCoeffs - нестандарт
 *2._data_/Body/AeroForceCoeffs, AeroMomentCoeffs - smooth вместо  smoothness
 *3. MeanProfileDrag
 *нужно указать что тестовые функции не парсят подобъект юнит
*/

#ifdef __cplusplus
}
#endif
