CONFIG += c++14
TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
TARGET = test_app
#QT       -= core gui

INCLUDEPATH += ../../inc

SOURCES += \
    ../../Examples/test_app/main.cpp


unix {
LIBS += -L../jdp/ -ljdp
QMAKE_LFLAGS += -Wl,-rpath,"'\$$ORIGIN'"
}
win32 {
    CONFIG(debug, debug|release) {
    LIBS += ../jdp/debug/libjdp.a
    }
    CONFIG(release, debug|release) {
    LIBS += ../jdp/release/libjdp.a
    }
}

win32 {
    CONFIG(debug, debug|release) {
        QMAKE_POST_LINK=echo f | xcopy /y  .\debug\test_app.exe ..\test_app.exe
    }
    CONFIG(release, debug|release) {
        QMAKE_POST_LINK=echo f | xcopy /y  .\release\test_app.exe ..\test_app.exe
    }
}
