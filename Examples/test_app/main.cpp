﻿#include "jdp.h"
#include <iostream>
#include "cwraptypes.h"
#include <string.h>
using namespace std;


#define JUST_DO_IT(x) do {\
    err = (x);\
    if(err)\
	goto error_handler;\
} while(false)

void* CDECL fn_malloc(uint32_t size)
{
    return ::malloc(size);
}

bool operator== (const EString &s1, const EString &s2)
{
    return s1.len == s2.len
	&& s1.bytes == s2.bytes
    && !s1.text  && !s2.text
    || s1.text && s2.text && !strcmp(s1.text, s2.text);
}

bool operator== (const EArrayDbl &a1, const EArrayDbl &a2)
{
    return a1.n == a2.n
    && !a1.values && !a2.values
	|| a1.values && a2.values
    && !memcmp(a1.values, a2.values, sizeof(double)*a1.n);
}

bool operator== (const ETableDbl &a1, const ETableDbl &a2)
{
    return a1.rows == a2.rows && a1.columns == a2.columns
    && !a1.table && !a2.table
	|| a1.table && a2.table
    && !memcmp(a1.table, a2.table, sizeof(double)*a1.rows*a1.columns);
}

bool operator== (const ERank3ArrayDbl &a1, const ERank3ArrayDbl &a2)
{
    return a1.rows == a2.rows && a1.columns == a2.columns && a1.pages == a2.pages
    && !a1.table && !a2.table
	    || a1.table && a2.table
    && !memcmp(a1.table, a2.table, sizeof(double)*a1.rows*a1.columns*a1.pages);
}

bool operator==(const EPoly2Dbl &o1, const EPoly2Dbl&o2)
{
    return !memcmp(&o1, &o2, sizeof(EPoly2Dbl));
}


bool operator==(const UnitsInertia &o1, const UnitsInertia&o2)
{
    return o1.inertia == o2.inertia;
}


bool operator==(const UnitsCombiTable &o1, const UnitsCombiTable&o2)
{
    return o1.arg1 == o2.arg1 && o1.arg2 == o2.arg2 && o1.table == o2.table;
}

bool operator==(const UnitsCombi3Table &o1, const UnitsCombi3Table&o2)
{
    return o1.arg1 == o2.arg1
	&& o1.arg2 == o2.arg2
	&& o1.arg3 == o2.arg3
	&& o1.table == o2.table;
}



bool operator== (const CombiTableDbl &o1, const CombiTableDbl &o2)
{
    return o1.smoothness == o2.smoothness
	&& o1.beyond == o2.beyond
	&& o1.arg1 == o2.arg1
	&& o1.arg1_title == o2.arg1_title
	&& o1.arg2 == o2.arg2
	&& o1.arg2_title == o2.arg2_title
	&& o1.table == o2.table
	&& o1.title == o2.title
	&& o1.units == o2.units;
}

bool operator== (const Combi3TableDbl &o1, const Combi3TableDbl &o2)
{
    return o1.smoothness == o2.smoothness
	&& o1.beyond == o2.beyond
	&& o1.arg1 == o2.arg1
	&& o1.arg1_title == o2.arg1_title
	&& o1.arg2 == o2.arg2
	&& o1.arg2_title == o2.arg2_title
	&& o1.arg3 == o2.arg3
	&& o1.arg3_title == o2.arg3_title
	&& o1.table == o2.table
	&& o1.title == o2.title
	&& o1.units == o2.units;
}

bool operator== (const UnitsPosition &o1, const UnitsPosition &o2)
{
    return o1.r_0 == o2.r_0 && o1.rotation == o2.rotation;
}


bool operator== (const T_position &o1, const T_position &o2)
{
    return !memcmp(o1.r_0, o2.r_0, sizeof(o1.r_0))
	&& !memcmp(o1.rotation, o2.rotation, sizeof(o1.rotation))
	&& o1.rotation_sequence == o2.rotation_sequence
	&& o1.rotation_type == o2.rotation_type
	&& o1.units == o2.units	;
}

bool operator== (const UnitsTwist &o1, const UnitsTwist &o2)
{
    return o1.factor == o2.factor;
}

bool operator== (const T_factor &o1, const T_factor &o2)
{
    return o1.factor == o2.factor 
	&& o1.type == o2.type
	&& o1.units == o2.units;
}

bool operator== (const T_VectorCombiTableDbl &o1, const T_VectorCombiTableDbl &o2)
{
    return o1.presnc == o2.presnc
	&& o1.X == o2.X
	&& o1.Y == o2.Y
	&& o1.Z == o2.Z;
}

bool operator==(const UnitsBladeSimple &o1, const UnitsBladeSimple &o2)
{
    return o1.r_0 == o2.r_0
	&& o1.length == o2.length
	&& o1.chord07 == o2.chord07
	&& o1.mass == o2.mass
	&& o1.gravity_moment == o2.gravity_moment
	&& o1.inertia == o2.inertia
	&& o1.tiploss_factor == o2.tiploss_factor
	&& o1.alpha_0 == o2.alpha_0
	&& o1.lift_curve_slope == o2.lift_curve_slope
	&& o1.lift_stall == o2.lift_stall
	&& o1.lock_number == o2.lock_number
	&& o1.drag == o2.drag
	&& o1.induction == o2.induction
	&& o1.profile == o2.profile;

}

bool operator==(const T_param_blade_simple &o1, const T_param_blade_simple &o2)
{
    return o1.presnc == o2.presnc
	&& o1.flex_type == o2.flex_type
	&& o1.r_0 == o2.r_0
	&& o1.length == o2.length
	&& o1.chord07 == o2.chord07
	&& o1.mass == o2.mass
	&& o1.gravity_moment == o2.gravity_moment
	&& o1.inertia == o2.inertia
	&& o1.twist == o2.twist
	&& o1.tiploss_factor == o2.tiploss_factor
	&& o1.lift_curve_slope == o2.lift_curve_slope
	&& o1.lift_stall == o2.lift_stall
	&& o1.alpha_0 == o2.alpha_0
	&& o1.lock_number == o2.lock_number
	&& o1.drag == o2.drag
	&& o1.induction == o2.induction
	&& o1.profile == o2.profile
	&& o1.units == o2.units;
}

bool operator==(const T_param_enginewithregulator &o1, const T_param_enginewithregulator &o2)
{
    return o1.n == o1.n
	&& o1.rpm_pace_accel == o1.rpm_pace_accel
	&& o1.rpm_pace_decel == o1.rpm_pace_decel
	&& o1.t1 == o1.t1
	&& o1.t2 == o1.t2
	&& o1.om1 == o1.om1
	&& o1.om2 == o1.om2
	&& o1.poly2_rpm_power == o1.poly2_rpm_power
	&& o1.power_hbar_temp_tbl == o1.power_hbar_temp_tbl;
}

bool operator==(const UnitsTransmission1M1T &o1, const UnitsTransmission1M1T &o2)
{
	return o1.power_trans_limit == o2.power_trans_limit
	    && o1.power_board_losses == o2.power_board_losses
	    && o1.power_main_use == o2.power_main_use
	    && o1.power_tail_use == o2.power_tail_use;
}

bool operator==(const T_param_transmission_1m1t_rotor &o1, const T_param_transmission_1m1t_rotor &o2)
{
    return o1.power_trans_limit == o2.power_trans_limit
	&& o1.power_board_losses == o2.power_board_losses
	&& o1.power_main_use == o2.power_main_use
	&& o1.power_tail_use == o2.power_tail_use
	&& o1.units == o2.units;
}

bool operator==(const UnitsHingeGeneral &o1, const UnitsHingeGeneral &o2)
{
    return o1.stop == o2.stop
	&& o1.angle == o2.angle
	&& o1.L_offset == o2.L_offset
	&& o1.damping == o2.damping
	&& o1.stiffness == o2.stiffness;
}

bool operator==(const T_param_hinge_general &o1, const T_param_hinge_general &o2)
{
    return o1.presnc == o2.presnc
	&& o1.fix_cond == o2.fix_cond
	&& o1.stop_min == o2.stop_min
	&& o1.stop_max == o2.stop_max
	&& o1.angle_0 == o2.angle_0
	&& o1.L_offset == o2.L_offset
	&& o1.damping == o2.damping
	&& o1.stiffness == o2.stiffness
	&& o1.units == o2.units;
}

bool operator==(const T_param_hub_simple &o1, const T_param_hub_simple &o2)
{
    return o1.presnc == o2.presnc
	&& o1.num_blades == o2.num_blades
	&& o1.inertia == o2.inertia
	&& o1.flap_hinge == o2.flap_hinge
	&& o1.lag_hinge == o2.lag_hinge
	&& o1.feather_hinge == o2.feather_hinge
	&& o1.units == o2.units;
}

bool operator==(const UnitsSwashplate &o1, const UnitsSwashplate &o2)
{
    return o1.d1 == o2.d1
	&& o1.d2 == o2.d2
	&& o1.lon_max == o2.lon_max
	&& o1.lon_min == o2.lon_min
	&& o1.lat_max == o2.lat_max
	&& o1.lat_min == o2.lat_min;
}

bool operator==(const T_param_swashplate &o1, const T_param_swashplate &o2)
{
    return o1.d1 == o2.d1
	&& o1.d2 == o2.d2
	&& o1.lon_max == o2.lon_max
	&& o1.lon_min == o2.lon_min
	&& o1.lat_max == o2.lat_max
	&& o1.lat_min == o2.lat_min
	&& o1.units == o2.units;
}

bool operator==(const T_param_cet_1m1t_rotor &o1, const T_param_cet_1m1t_rotor &o2)
{
    return o1.ct_0 == o2.ct_0
	&& o1.ct_oz == o2.ct_oz
	&& o1.ct_ox == o2.ct_ox

	&& o1.ch_0 == o2.ch_0
	&& o1.ch_oz == o2.ch_oz
	&& o1.ch_ox == o2.ch_ox

	&& o1.cs_0 == o2.cs_0
	&& o1.cs_oz == o2.cs_oz
	&& o1.cs_ox == o2.cs_ox

	&& o1.cmx_0 == o2.cmx_0
	&& o1.cmx_oz == o2.cmx_oz
	&& o1.cmx_ox == o2.cmx_ox

	&& o1.cmz_0 == o2.cmz_0
	&& o1.cmz_oz == o2.cmz_oz
	&& o1.cmz_ox == o2.cmz_ox

	&& o1.cmk_0 == o2.cmk_0
	&& o1.cmk_oz == o2.cmk_oz
	&& o1.cmk_ox == o2.cmk_ox
	&& o1.cmk_om == o2.cmk_om

	&& o1.tr_ct == o2.tr_ct
	&& o1.tr_ch == o2.tr_ch
	&& o1.tr_cs == o2.tr_cs
	&& o1.tr_cmk == o2.tr_cmk
	&& o1.Omg_con == o2.Omg_con;
}

bool operator==(const UnitsRotorSimple &o1, const UnitsRotorSimple &o2)
{
    return o1.direction == o2.direction
	&& o1.r == o2.r
	&& o1.pitch_flap_coupling == o2.pitch_flap_coupling
	&& o1.omega_nom == o2.omega_nom
	&& o1.omega_min == o2.omega_min
	&& o1.omega_ratio == o2.omega_ratio;
}
bool operator==(const T_param_rotor_simple &o1, const T_param_rotor_simple &o2)
{
    return o1.presnc == o2.presnc
	&& o1.direction == o2.direction
	&& o1.r == o2.r
	&& o1.pitch_flap_coupling == o2.pitch_flap_coupling
	&& o1.omega_nom == o2.omega_nom
	&& o1.omega_min == o2.omega_min
	&& o1.omega_ratio == o2.omega_ratio
	&& o1.type == o2.type
	&& o1.hub == o2.hub
	&& o1.blade == o2.blade
	&& o1.swashplate == o2.swashplate
	&& o1.mean_profile_drag == o2.mean_profile_drag
	&& o1.units == o2.units;
}

bool operator==(const UnitsHTail &o1, const UnitsHTail &o2)
{
    return o1.span == o2.span
	&& o1.v_shape == o2.v_shape
	&& o1.area == o2.area;
}

bool operator==(const T_param_H_tail &o1, const T_param_H_tail &o2)
{
    return o1.presnc == o2.presnc
	&& o1.span == o2.span
	&& o1.v_shape == o2.v_shape
	&& o1.area == o2.area
	&& o1.excentr == o2.excentr
	&& !memcmp(o1.c_press, o2.c_press, sizeof(o1.c_press))
	&& !memcmp(o1.kinematics, o2.kinematics, sizeof(o1.kinematics))
	&& o1.position == o2.position
	&& o1.aero_force_coeffs == o2.aero_force_coeffs
	&& o1.aero_moment_coeffs == o2.aero_moment_coeffs
	&& o1.units == o2.units;
}

bool operator==(const UnitsVTail &o1, const UnitsVTail &o2)
{
    return o1.v_shape == o2.v_shape
	&& o1.area == o2.area;
}

bool operator==(const T_param_V_tail &o1, const T_param_V_tail &o2)
{
    return o1.presnc == o2.presnc
	&& o1.nsurfs == o2.nsurfs
	&& !memcmp(o1.symmplane, o2.symmplane, sizeof(o1.symmplane))
	&& o1.area == o2.area
	&& o1.area_flow == o2.area_flow
	&& o1.v_shape == o2.v_shape
	&& !memcmp(o1.c_press, o2.c_press, sizeof(o1.c_press))
	&& o1.position == o2.position
	&& o1.aero_force_coeffs == o2.aero_force_coeffs
	&& o1.units == o2.units;
}

bool operator==(const UnitsFuselageSimple &o1, const UnitsFuselageSimple &o2)
{
    return o1.length == o2.length
	&& o1.cross_area == o2.cross_area
	&& o1.inertia == o2.inertia;
}

bool operator==(const T_param_fuselage_simple &o1, const T_param_fuselage_simple &o2)
{
    return o1.length == o2.length
	&& o1.cross_area == o2.cross_area
	&& !memcmp(o1.inertia, o2.inertia, sizeof(o1.inertia))
	&& o1.aero_force_coeffs == o2.aero_force_coeffs
	&& o1.aero_moment_coeffs == o2.aero_moment_coeffs
	&& o1.units == o2.units;
}

bool operator==(const T_param_simple_one_rotor_helicopter &o1, const T_param_simple_one_rotor_helicopter &o2)
{
    return o1.TOW == o2.TOW
	&& !memcmp(o1.CG, o2.CG, sizeof(o1.CG))
	&& o1.Body == o2.Body
	&& o1.MainRotor == o2.MainRotor
	&& o1.MainRotor_Position == o2.MainRotor_Position
	&& o1.TailRotor == o2.TailRotor
	&& o1.TailRotor_Position == o2.TailRotor_Position
	&& o1.Transmission == o2.Transmission
	&& o1.HTail == o2.HTail
	&& o1.VTail == o2.VTail
	&& o1.OnBodyFlowLosses == o2.OnBodyFlowLosses;
}

T_param_simple_one_rotor_helicopter top_level_obj;

bool parse_v_tail(void* parser)
{
    std::cout << "parse_v_tail" << std::endl;
    void*p = (void*)&top_level_obj;
    //uint32_t len = sizeof(top_level_obj);
    //res = parse_sub_level_object(parser, TypeIds::T_position, "/MainRotorPosition", p);
    auto res = parse_object(parser, TypeIds::param_V_tail, "/_data_/V_Tail", p);
    std::cout << "parse_sub_level_object: " << res << std::endl;
    if (res == Error::NoError)
    {
	auto vt = static_cast<T_param_V_tail*>(p);
	std::cout <<"presnc: " << hex << vt->presnc << endl;
	std::cout << "nsurfs: " << vt->nsurfs << endl;
	std::cout << "symmplane: " << vt->symmplane[0] << vt->symmplane[1] << vt->symmplane[2] << endl;
	std::cout << "area: " << vt->area << endl;
	std::cout << "area_flow: " << vt->area_flow << endl; 
    	std::cout << "v_shape: " << vt->v_shape << endl;
	std::cout << "c_press: " << vt->c_press[0] <<" " << vt->c_press[1] << " " << vt->c_press[2] << endl;
	return true;

	//auto p_position = static_cast<T_param_V_tail*>(p);
	//std::cout << "rotation_type: " << p_position->rotation_type.text << std::endl;
	//std::cout << "rotation_sequence: " << p_position->rotation_sequence.text << std::endl;
	//std::cout << "r_0: " << std::endl;
	//for (unsigned i = 0; i<3; i++)
	//{
	//    std::cout << p_position->r_0[i] << std::endl;
	//}
	//std::cout << "rotation: " << std::endl;
	//for (unsigned i = 0; i<3; i++)
	//{
	//    std::cout << p_position->rotation[i] << std::endl;
	//}
    }
    return false;
}

bool parse_top_obj(void*parser)
{
    void*p = (void*)&top_level_obj;
    auto res = parse_object(parser, TypeIds::param_one_rotor_helicopter, "/_data_", p);
    std::cout << "parse_top_obj: " << res << std::endl;
    if (res == Error::NoError)
    {
    //auto o = static_cast<T_param_simple_one_rotor_helicopter*>(p);

	return true;

	//auto p_position = static_cast<T_param_V_tail*>(p);
	//std::cout << "rotation_type: " << p_position->rotation_type.text << std::endl;
	//std::cout << "rotation_sequence: " << p_position->rotation_sequence.text << std::endl;
	//std::cout << "r_0: " << std::endl;
	//for (unsigned i = 0; i<3; i++)
	//{
	//    std::cout << p_position->r_0[i] << std::endl;
	//}
	//std::cout << "rotation: " << std::endl;
	//for (unsigned i = 0; i<3; i++)
	//{
	//    std::cout << p_position->rotation[i] << std::endl;
	//}
    }
    return false;
}

int test_parse_example_json(int argc, char** argv)
{
    if (argc < 2)
	return 1;
    void* parser = nullptr;
    auto res = init_parser_from_file(argv[1], &parser);
    std::cout << "init_parser_from_file: " << res << std::endl;
    if(res == Error::NoError)
    {
	set_alloc(parser, fn_malloc);
	void*p = (void*)&top_level_obj;
	if(!parse_top_obj(parser))
	{
	    auto res = parse_object(parser, TypeIds::param_fuselage, "/_data_/Body", p);
	    std::cout << "Body: " << res << std::endl;
	    if (res == Error::NoError)
	    {
		res = parse_object(parser, TypeIds::param_rotor_simple, "/_data_/MainRotor", p);
		std::cout << "MainRotor: " << res << std::endl;
		if (res == Error::NoError)
		{
		    res = parse_object(parser, TypeIds::T_position, "/_data_/MainRotorPosition", p);
		    std::cout << "MainRotorPosition: " << res << std::endl;
		    if (res == Error::NoError)
		    {
			res = parse_object(parser, TypeIds::param_rotor_simple, "/_data_/TailRotor", p);
			std::cout << "TailRotor: " << res << std::endl;
			if (res == Error::NoError)
			{
			    res = parse_object(parser, TypeIds::T_position, "/_data_/TailRotorPosition", p);
			    std::cout << "TailRotorPosition: " << res << std::endl;
			    if (res == Error::NoError)
			    {
				res = parse_object(parser, TypeIds::param_transmission_1m1t_rotor, "/_data_/Transmission", p);
				std::cout << "Transmission: " << res << std::endl;
				if (res == Error::NoError)
				{
				    res = parse_object(parser, TypeIds::param_H_tail, "/_data_/H_Tail", p);
				    std::cout << "H_Tail: " << res << std::endl;
				    if (res == Error::NoError)
				    {
					res = parse_object(parser, TypeIds::param_V_tail, "/_data_/V_Tail", p);
					std::cout << "V_Tail: " << res << std::endl;
					if (res == Error::NoError)
					{

					}
				    }
				}
			    }
			}
		    }
		}
	    }
	} 
    }

	fini_parser(parser);
	return res;
}

struct tests_t
{
    void* parser = nullptr;
    bool verbose = false;
    int passed = 0;
    int failed = 0;

    Error::Code err;
    Error::Code setup_snippet(const char* snippet)
    {

	JUST_DO_IT(test_init_parser_from_string(snippet, &parser));
	set_alloc(parser, fn_malloc);

	error_handler:
	return err;
    }
    static void println(const char*s)
    {
	std::cout << s << endl;
    }
    static void print(const char*s)
    {
	std::cout << s << ": ";
    }

    void test_fail()
    {
        failed ++;
        std::cout << "FAIL" << endl;
    }


    void test_ok()
    {
        passed ++;
        std::cout << "PASS" << endl;
    }


    void test_EString()
    {
	const char *snippet = u8"\"проверка\"";
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	EString so;
	JUST_DO_IT(test_parse_string(parser, &so));
	if (so.len != 8)
	    goto error_handler;
	if(so.bytes != 16)
	    goto error_handler;
	if(strcmp(so.text, u8"проверка"))
	    goto error_handler;
	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_array_dbl_1x3()
    {
	const char* snippet = "[4502.0, -841.0, 0.0 ]";
	double expected[] = { 4502.0, -841.0, 0.0 };
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	double o[3];
	JUST_DO_IT(test_parse_1x3_dbl_array(parser, &o));

	if (memcmp(expected, o, sizeof(expected)))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_array_dbl_1x3_null()
    {
	const char* snippet = "null";
	double expected[] = { 0.0, 0.0, 0.0 };
	double o[3] = { 1,2,3 };
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));

	JUST_DO_IT(test_parse_1x3_dbl_array(parser, &o));

	if (memcmp(expected, o, sizeof(expected)))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_array_dbl_1x3_non_array()
    {
	const char* snippet = "123";
	double o[3];
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));

	err = (test_parse_1x3_dbl_array(parser, &o));
	if (err != Error::ArrayDbl1x3_IsNotArray)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_array_dbl_1x3_size_mismatch()
    {
	const char* snippet = "[1,2]";
	double o[3];

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));

	err = (test_parse_1x3_dbl_array(parser, &o));
	if (err != Error::ArrayDbl1x3_Size_is_not_3)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_array_dbl_1x3_all_elements_null()
    {
	const char* snippet = "[null,null,null]";
	double expected[] = { 0.0, 0.0, 0.0 };
	double o[3];

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));

	JUST_DO_IT(test_parse_1x3_dbl_array(parser, &o));

	if (memcmp(expected, o, sizeof(expected)))
	    goto error_handler;


	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_array_dbl_1x3_element_type_mismatch()
    {
	const char* snippet = "[true, 2, 3 ]";
	double o[3];

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));

	err = (test_parse_1x3_dbl_array(parser, &o));
	if (err != Error::ArrayDbl1x3_ElementIsNotNumber)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_array_dbl_3x3()
    {
	const char* snippet = "[ [4502.0, -841.0, 0.0 ], [ -841.0, 24500.0, 0.0 ], [ 0.0, 0.0, 18500.0 ] ]";
	T_inertia expected = {
		{ 4502.0, -841.0, 0.0 },
		{ -841.0, 24500.0, 0.0 },
		{ 0.0, 0.0, 18500.0 }
	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	T_inertia o;
	JUST_DO_IT(test_parse_3x3_dbl_array(parser, &o));

	if (memcmp(expected, o, sizeof(expected)))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_array_dbl_3x3_all_integers()
    {
	const char* snippet = "[ [4502, -841, 0 ], [ -841, 24500, 0 ], [ 0, 0, 18500 ] ]";
	T_inertia expected = {
	{ 4502.0, -841.0, 0.0 },
	{ -841.0, 24500.0, 0.0 },
	{ 0.0, 0.0, 18500.0 }
	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	T_inertia o;
	JUST_DO_IT(test_parse_3x3_dbl_array(parser, &o));

	if (memcmp(expected, o, sizeof(expected)))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_array_dbl_3x3_null()
    {
	const char* snippet = "null";
	T_inertia expected = { { 0.0, 0.0, 0.0 },{ 0.0, 0.0, 0.0 },{ 0.0, 0.0, 0.0 } };
	T_inertia o;
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));

	JUST_DO_IT(test_parse_3x3_dbl_array(parser, &o));

	if (memcmp(expected, o, sizeof(expected)))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_array_dbl_3x3_size_mismatch()
    {
	const char* snippet = "[ [4502.0, -841.0, 0.0 ], [ -841.0, 24500.0, 0.0 ] ]";
	print(__FUNCTION__);
	T_inertia o;
	JUST_DO_IT(setup_snippet(snippet));

	err = test_parse_3x3_dbl_array(parser, &o);
	if (err != Error::ArrayDbl3x3_Size_is_not_3)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_array_dbl_3x3_all_subarrays_null()
    {
	const char* snippet = "[ null, null, null ]";
	T_inertia expected = {
	    { 0.0, 0.0, 0.0 },
	{ 0.0, 0.0, 0.0 },
	{ 0.0, 0.0, 0.0 }
	};
	T_inertia o;
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	JUST_DO_IT(test_parse_3x3_dbl_array(parser, &o));

	if (memcmp(expected, o, sizeof(expected)))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_array_dbl_3x3_non_array()
    {
	const char* snippet = "\"test\"";
	T_inertia o;
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));

	err = (test_parse_3x3_dbl_array(parser, &o));
	if (err != Error::ArrayDbl3x3_IsNotArray)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_array_char_1x3()
    {
	const char* snippet = u8R"("123")";
	char expected[] = { '1','2','3' };
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	char o[3];
	JUST_DO_IT(test_parse_1x3_char_array(parser, &o));

	if (memcmp(expected, o, sizeof(expected)))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_array_char_1x3_null()
    {
	const char* snippet = "null";
	char expected[] = { 0,0,0 };
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	char o[3];
	JUST_DO_IT(test_parse_1x3_char_array(parser, &o));

	if (memcmp(expected, o, sizeof(expected)))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_array_char_1x3_non_string()
    {
	const char* snippet = "[1,2,3]";
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	char o[3];
	err = (test_parse_1x3_char_array(parser, &o));
	if (err != Error::ArrayChar1x3_IsNotString)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_array_char_1x3_char_count_gt_3()
    {
	const char* snippet = u8R"("1234")";
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	char o[3];
	err = (test_parse_1x3_char_array(parser, &o));
	if (err != Error::ArrayChar1x3_Size_gt_3)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_array_char_1x3_byte_count_gt_3()
    {
	const char* snippet = u8R"("цыц")";
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	char o[3];
	err = (test_parse_1x3_char_array(parser, &o));
	if (err != Error::ArrayChar1x3_Size_gt_3)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_array_char_1x3_size_lt_3()
    {
	const char* snippet = u8R"("12")";
	char expected[] = { '1','2',0 };
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	char o[3];
	JUST_DO_IT(test_parse_1x3_char_array(parser, &o));

	if (memcmp(expected, o, sizeof(expected)))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_EArrayDbl()
    {
	const char* snippet = "[-180.00,-100.00,80.00,180.00]";
	double expected[] = { -180.00,-100.00,80.00,180.00 };
	EArrayDbl o;
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	JUST_DO_IT(test_parse_EArrayDbl(parser, &o));
	if (o.n != 4)
	    goto error_handler;
	if (memcmp(expected, o.values, sizeof(expected)))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_EArrayDbl_null()
    {
	const char* snippet = "null";
	EArrayDbl o;
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	JUST_DO_IT(test_parse_EArrayDbl(parser, &o));
	if (o.n != 0 || o.values != nullptr)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }


    void test_EArrayDbl_empty()
    {
	const char* snippet = "[]";

	EArrayDbl o;
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	JUST_DO_IT(test_parse_EArrayDbl(parser, &o));
	if (o.n != 0 || o.values != nullptr)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_EArrayDbl_non_array()
    {
	const char* snippet = R"("test")";

	EArrayDbl o;
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	err = (test_parse_EArrayDbl(parser, &o));
	if (err != Error::JsonFieldTypeMismatch)
	    goto error_handler;
	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_EArrayDbl_heterogeneous()
    {
	const char* snippet = "[0, true]";

	EArrayDbl o;
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	err = (test_parse_EArrayDbl(parser, &o));
	if (err != Error::EArrayDbl_InappropriateElementType)
	    goto error_handler;
	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_EArrayDbl_elements_all_null()
    {
	const char* snippet = "[null, null, null, null]";

	double expected[] = { 0.0, 0.00, 0.00, 0.00 };
	EArrayDbl o;
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	JUST_DO_IT(test_parse_EArrayDbl(parser, &o));
	if (o.n != 4)
	    goto error_handler;
	if (memcmp(expected, o.values, sizeof(expected)))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_EArrayDbl_element_type_mismatch()
    {
	const char* snippet = "[true, 2, 3, \"4\"]";

	EArrayDbl o;
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	err = (test_parse_EArrayDbl(parser, &o));
	if (err != Error::EArrayDbl_InappropriateElementType)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ETableDbl()
    {
	const char* snippet = u8R"([[0.0200E+00,0.0200E+00,0.0200E+00,0.0200E+00],[2.0000E-03,1.0000E-03,1.1500E-01,2.0000E-03],[1.1000E-02,8.3300E-01,8.7800E-01,1.1000E-02],[0.0200E+00,0.0200E+00,0.0200E+00,0.0200E+00]])";
	double doubles[4][4] = { 
		{ 0.0200E+00,0.0200E+00,0.0200E+00,0.0200E+00 },
		{ 2.0000E-03,1.0000E-03,1.1500E-01,2.0000E-03 },
		{ 1.1000E-02,8.3300E-01,8.7800E-01,1.1000E-02 },
		{ 0.0200E+00,0.0200E+00,0.0200E+00,0.0200E+00 }
	};

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	ETableDbl o;
	JUST_DO_IT(test_parse_ETableDbl(parser, &o));
	if (o.rows != 4)
	    goto error_handler;
	if (o.columns != 4)
	    goto error_handler;
	if (memcmp(doubles, o.table, sizeof(doubles)))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ETableDbl_null()
    {
	const char* snippet = "null";

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	ETableDbl o;
	JUST_DO_IT(test_parse_ETableDbl(parser, &o));
	if (o.rows != 0 || o.columns != 0 || o.table != nullptr)
	    goto error_handler;

    	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ETableDbl_empty()
    {
	const char* snippet = "[]";

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	ETableDbl o;
	JUST_DO_IT(test_parse_ETableDbl(parser, &o));
	if (o.rows != 0 || o.columns != 0 || o.table != nullptr)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ETableDbl_all_subarrays_null()
    {
	const char* snippet = "[null, null]";
	ETableDbl o;

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	err = (test_parse_ETableDbl(parser, &o));
	if (err != Error::ETblDbl_AllSubArraysAreNullObjects)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ETableDbl_diff_size_subarrays_expected_fail()
    {
	const char* snippet = "[ [1, 2], [3 ] ]";
	print(__FUNCTION__);
	ETableDbl o;
	JUST_DO_IT(setup_snippet(snippet));
	err = (test_parse_ETableDbl(parser, &o));
	if (err != Error::ETblDbl_SubArraySizeMismatch)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ETableDbl_null_subarray()
    {
	const char* snippet = "[ [1, 2], null ]";
	double expected[2][2] = { { 1, 2 }, { 0, 0 }, };
	print(__FUNCTION__);
	ETableDbl o;
	JUST_DO_IT(setup_snippet(snippet));
	JUST_DO_IT(test_parse_ETableDbl(parser, &o));
	if (o.columns != 2 && o.rows != 2)
	    goto error_handler;
	if (memcmp(expected, o.table, sizeof(expected)))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ETableDbl_subarray_non_array()
    {
	const char* snippet = "[null, true]";
	ETableDbl o;

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	err = (test_parse_ETableDbl(parser, &o));
	if (err != Error::ETblDbl_SubArrayIsNotArray)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ETableDbl_all_subarray_empty()
    {
	const char* snippet = "[ [], [] ]";
	print(__FUNCTION__);
	ETableDbl o;
	JUST_DO_IT(setup_snippet(snippet));
	err = (test_parse_ETableDbl(parser, &o));
	if (o.rows != 2 && o.columns != 0 && o.table != nullptr)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ETableDbl_all_elements_null()
    {
	const char* snippet = "[ [null,null], [null,null] ]";
	double expected[2][2] = { { 0, 0 },{ 0, 0 }, };
	print(__FUNCTION__);
	ETableDbl o;

	JUST_DO_IT(setup_snippet(snippet));
	JUST_DO_IT(test_parse_ETableDbl(parser, &o));
	if (o.columns != 2 && o.rows != 2)
	    goto error_handler;
	if (memcmp(expected, o.table, sizeof(expected)))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ETableDbl_element_type_mismatch()
    {
	const char* snippet = "[[true,2], [3,4]]";
	ETableDbl o;

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	err = (test_parse_ETableDbl(parser, &o));
	if (err != Error::ETblDbl_LeafElementIsNotNumber)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ERank3ArrayDbl()
    {
	const char* snippet = "[ [ [1,2], [3,4] ], [[5,6], [7,8]] ]";
	double expacted[2][2][2] = {
	    {{1,2}, {3,4}},
	{ { 5,6 },{ 7,8 } },
	};

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	ERank3ArrayDbl o;
	JUST_DO_IT(test_parse_ERank3ArrayDbl(parser, &o));
	if (o.rows != 2 || o.columns != 2 || o.pages != 2 )
	    goto error_handler;

	if (memcmp(expacted, o.table, sizeof(expacted)))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ERank3ArrayDbl_null()
    {
	const char* snippet = "null";

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	ERank3ArrayDbl o;
	JUST_DO_IT(test_parse_ERank3ArrayDbl(parser, &o));
	if (o.rows != 0 || o.columns != 0 || o.pages != 0 || o.table != nullptr)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ERank3ArrayDbl_non_array()
    {
	const char* snippet = "{}";

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	ERank3ArrayDbl o;
	err = (test_parse_ERank3ArrayDbl(parser, &o));
	if (err != Error::ER3ADbl_IsNotArray)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ERank3ArrayDbl_empty()
    {
	const char* snippet = "[]";

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	ERank3ArrayDbl o;
	JUST_DO_IT(test_parse_ERank3ArrayDbl(parser, &o));
	if (o.rows != 0 || o.columns != 0 || o.pages != 0 || o.table != nullptr)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ERank3ArrayDbl_subarray_non_array()
    {
	const char* snippet = "[ [ [1,2], [3,4] ], {  } ]";


	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	ERank3ArrayDbl o;
	err = (test_parse_ERank3ArrayDbl(parser, &o));
	if(err != Error::ER3ADbl_SubArray_IsNotArray)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ERank3ArrayDbl_subsubarray_non_array()
    {
	const char* snippet = "[ [ [1,2], [3,4] ], [ [5,6], {} ] ]";

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	ERank3ArrayDbl o;
	err = (test_parse_ERank3ArrayDbl(parser, &o));
	if (err != Error::ER3ADbl_SubSubArray_IsNotArray)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ERank3ArrayDbl_subsubarray_size_mismatch()
    {
	const char* snippet = "[ [ [1,2], [3,4] ], [ [5,6], [7] ] ]";


	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	ERank3ArrayDbl o;
	err = (test_parse_ERank3ArrayDbl(parser, &o));
	if (err != Error::ER3ADbl_SubSubArraySizeMismatch)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ERank3ArrayDbl_subarray_size_mismatch()
    {
	const char* snippet = "[ [ [1,2], [3,4] ], [ [5,6]  ] ]";

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	ERank3ArrayDbl o;
	err = (test_parse_ERank3ArrayDbl(parser, &o));
	if (err != Error::ER3ADbl_SubArraySizeMismatch)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ERank3ArrayDbl_all_subsubarrays_null()
    {
	const char* snippet = "[ [ null, null ], [ null, null ] ]";

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	ERank3ArrayDbl o;
	err = (test_parse_ERank3ArrayDbl(parser, &o));
	if (err != Error::ER3ADbl_AllSubSubArraysAreNullObjects)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ERank3ArrayDbl_all_subarrays_null()
    {
	const char* snippet = "[ null, null ]";

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	ERank3ArrayDbl o;
	err = (test_parse_ERank3ArrayDbl(parser, &o));
	if (err != Error::ER3ADbl_AllSubArraysAreNullObjects)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ERank3ArrayDbl_element_non_number()
    {
	const char* snippet = "[ [ [true,2], [3,4] ], [[5,6], [7,8]] ]";

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	ERank3ArrayDbl o;
	err = (test_parse_ERank3ArrayDbl(parser, &o));
	if (err != Error::ER3ADbl_LeafElementIsNotNumber)
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_ERank3ArrayDbl_null_subarray()
    {
	const char* snippet = "[ [ [1,2], [3,4] ], null ]";

	double expacted[2][2][2] = {
	    { { 1,2 },{ 3,4 } },
	{ { 0,0 },{ 0,0 } },
	};

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	ERank3ArrayDbl o;
	JUST_DO_IT(test_parse_ERank3ArrayDbl(parser, &o));
	if (o.rows != 2 || o.columns != 2 || o.pages != 2)
	    goto error_handler;

	if (memcmp(expacted, o.table, sizeof(expacted)))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ERank3ArrayDbl_null_subsubarray()
    {
	const char* snippet = "[ [ [1,2], null ], [null, null] ]";

	double expacted[2][2][2] = {
	    { { 1,2 },{ 0,0 } },
	{ { 0,0 },{ 0,0 } },
	};

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	ERank3ArrayDbl o;
	JUST_DO_IT(test_parse_ERank3ArrayDbl(parser, &o));
	if (o.rows != 2 || o.columns != 2 || o.pages != 2)
	    goto error_handler;

	if (memcmp(expacted, o.table, sizeof(expacted)))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_ERank3ArrayDbl_all_elements_null()
    {
	const char* snippet = "[ [ [null,null], [null,null] ], [[null,null], [null,null]] ]";

	double expacted[2][2][2] = {
	    { { 0,0 },{ 0,0 } },
	{ { 0,0 },{ 0,0 } },
	};

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	ERank3ArrayDbl o;
	JUST_DO_IT(test_parse_ERank3ArrayDbl(parser, &o));
	if (o.rows != 2 || o.columns != 2 || o.pages != 2)
	    goto error_handler;

	if (memcmp(expacted, o.table, sizeof(expacted)))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_EPoly2Dbl()
    {
	const char* snippet = R"({"a0": 0.002467, "a1":-0.3914, "a2":15.5044})";
	EPoly2Dbl expected = { 0.002467,-0.3914,15.5044 };

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	EPoly2Dbl o;
	JUST_DO_IT(test_parse_EPoly2Dbl(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_UnitsInertia()
    {
	const char* snippet = R"({"Inertia": "kg"})";
	UnitsInertia expected = { {const_cast<char*>("kg"),2,2} };
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	UnitsInertia o;
	JUST_DO_IT(test_parse_UnitsInertia(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_UnitsCombiTable()
    {
	const char* snippet = R"({"arg1": "-","arg2": "1","table": "-"})";
	UnitsCombiTable expected = { 
		{ const_cast<char*>("-"),1,1 },
	{ const_cast<char*>("1"),1,1 },
	{ const_cast<char*>("-"),1,1 },
	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	UnitsCombiTable o;
	JUST_DO_IT(test_parse_UnitsCombiTable(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_UnitsCombi3Table()
    {
	const char* snippet = R"({"arg1": "-","arg2": "1","table": "-", "arg3": "?"})";
	UnitsCombi3Table expected = {
	    { const_cast<char*>("-"),1,1 },
	    { const_cast<char*>("1"),1,1 },
	    { const_cast<char*>("?"),1,1 },
	    { const_cast<char*>("-"),1,1 },
	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	UnitsCombi3Table o;
	JUST_DO_IT(test_parse_UnitsCombi3Table(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_CombiTableDbl()
    {
	const char* snippet = R"({
					"title":"C_yf",
					"table": [
					[-5.00E-01,	-5.00E-01,	-5.00E-01,	-5.00E-01,	-5.00E-01],
					[-2.00E-01,	-2.00E-01,	-3.00E-01,	-2.00E-01,	-2.00E-01],
					[-3.00E-01,	-4.00E-01,	-1.00E-01,	-2.00E-01,	-3.00E+00],
					[0.00E+00,	3.00E-01,	-2.00E-01,	-6.00E-01,	0.00E+00],
					[2.00E-01,	3.00E-01,	1.00E-01,		-1.00E-01,	2.00E-01],
					[2.00E-01,	3.00E-01,	-4.00E-01,	-1.00E-01,	2.00E-01]
					],
					"arg1_title":"alpha_f",
					"arg1":[-180.00,	-60.00,	0.00,	90.00, 180.00],
					"arg2_title":"beta_f",
					"arg2":[-90.00,	-40.00,	-15.00,	0.00,	30.00, 90.00],
					"smoothness":"interp",
					"beyond":"circle",
					"units" : {
						"table":"-",
						"arg1":"deg",
						"arg2":"deg"
					}
				})";

	double arg1[] = { -180.00,	-60.00,	0.00,	90.00, 180.00 };
	double arg2[] = { -90.00,	-40.00,	-15.00,	0.00,	30.00, 90.00 };
	double table[6][5] = { 
	    { -5.00E-01,	-5.00E-01,	-5.00E-01,	-5.00E-01,	-5.00E-01 },
	{ -2.00E-01,	-2.00E-01,	-3.00E-01,	-2.00E-01,	-2.00E-01 },
	{ -3.00E-01,	-4.00E-01,	-1.00E-01,	-2.00E-01,	-3.00E+00 },
	{ 0.00E+00,	3.00E-01,	-2.00E-01,	-6.00E-01,	0.00E+00 },
	{ 2.00E-01,	3.00E-01,	1.00E-01,		-1.00E-01,	2.00E-01 },
	{ 2.00E-01,	3.00E-01,	-4.00E-01,	-1.00E-01,	2.00E-01 }
	};
	CombiTableDbl expected = {
	    { const_cast<char*>("interp"),6,6 },
	    { const_cast<char*>("circle"),6,6 },
	    { arg1, sizeof(arg1)/sizeof(double) },
	{ const_cast<char*>("alpha_f"),7,7 },
	{ arg2, sizeof(arg2) / sizeof(double) },
	{ const_cast<char*>("beta_f"),6,6 },
	{ (double*)table, 6, 5},
	{ const_cast<char*>("C_yf"),4,4 },
	{    
	    { const_cast<char*>("deg"),3,3 }, 
	    { const_cast<char*>("deg"),3,3 },
	    { const_cast<char*>("-"),1,1 } 
	}
	};

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	CombiTableDbl o;
	JUST_DO_IT(test_parse_CombiTableDbl(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_Combi3TableDbl()
    {
	const char* snippet = R"({
					"title":"C_yf",
					"table":  [[ [1,2], [3,4] ], [[5,6], [7,8]]],
					"arg1_title":"alpha_f",
					"arg1":[-180.00,	-60.00,	0.00,	90.00, 180.00],
					"arg2_title":"beta_f",
					"arg2":[-90.00,	-40.00,	-15.00,	0.00,	30.00, 90.00],
					"arg3_title":"delta_f",
					"arg3":[1,	2,	3,	4,	5, 6],
					"smoothness":"interp",
					"beyond":"circle",
					"units" : {
						"table":"-",
						"arg1":"deg",
						"arg2":"deg",
						"arg3":"deg"
					}
				})";

	double arg1[] = { -180.00,	-60.00,	0.00,	90.00, 180.00 };
	double arg2[] = { -90.00,	-40.00,	-15.00,	0.00,	30.00, 90.00 };
	double arg3[] = { 1,	2,	3,	4,	5, 6 };
	double table[2][2][2] = {
	    {{1,2}, {3,4} },
		{{5,6}, {7,8}}
	};
	Combi3TableDbl expected = {
	    { const_cast<char*>("interp"),6,6 },
	{ const_cast<char*>("circle"),6,6 },
	{ arg1, sizeof(arg1) / sizeof(double) },
	{ const_cast<char*>("alpha_f"),7,7 },
	{ arg2, sizeof(arg2) / sizeof(double) },
	{ const_cast<char*>("beta_f"),6,6 },
	{ arg3, sizeof(arg3) / sizeof(double) },
	{ const_cast<char*>("delta_f"),7,7 },
	{ (double*)table, 2, 2,2 },
	{ const_cast<char*>("C_yf"),4,4 },
	{
	    { const_cast<char*>("deg"),3,3 },
	    { const_cast<char*>("deg"),3,3 },
	{ const_cast<char*>("deg"),3,3 },
	    { const_cast<char*>("-"),1,1 } 
	}
	};

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	Combi3TableDbl o;
	JUST_DO_IT(test_parse_Combi3TableDbl(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;

    }
    void test_UnitsPosition()
    {
	const char* snippet = R"({
			"r_0": "m",
			"rotation": "rad"
		})";
	UnitsPosition expected = {
	    { const_cast<char*>("m"),1,1 },
	{ const_cast<char*>("rad"),3,3 },
	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	UnitsPosition o;
	JUST_DO_IT(test_parse_UnitsPosition(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_T_position()
    {
	const char* snippet = R"({
				"$ref_units_": "#/_units_/_position_",
				"r_0":[ -6.0, -0.0, 0.0],
				"rotationType":"axesRotation",
				"rotationSequence":		"ZXY",
				"rotation":	[0.0, 0.0, 0.0]
			})";
	T_position expected = {
	    { -6.0, -0.0, 0.0 },
	    { 0.0, 0.0, 0.0 },
	{ const_cast<char*>("axesRotation"),12,12 },
	{ const_cast<char*>("ZXY"),3,3 },
    {}
	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	T_position o;
	JUST_DO_IT(test_parse_T_position(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_T_factor()
    {
	const char* snippet = R"({
					"$ref_units_": "#/_units_/_twist_",
					"type":"tip-linear",
					"factor": -0.06
				})";
	T_factor expected = {
		-0.06,
		{ const_cast<char*>("tip-linear"),10,10 },
    {}
	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	T_factor o;
	JUST_DO_IT(test_parse_T_factor(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_T_VectorCombiTableDbl()
    {
	const char* snippet = R"({
				"Y": {
					"title":"C_yht",
					"arg1": [-180.00,	-100.00, 80.00,	180.00],
					"arg1_title":"alpha_ht",
					"arg2": [-90.00,	-30.00,	30.00,	90.00],
					"arg2_title":"beta_ht",
					"table":[
					[0.0000E+00,	0.0000E+00,	0.0000E+00,	0.0000E+00],
					[2.0000E-03,	1.0000E-03,	1.1500E-01,	2.0000E-03],
					[1.1000E-02,	8.3300E-01,	8.7800E-01,	1.1000E-02],
					[0.0000E+00,  0.0000E+00,	0.0000E+00,	0.0000E+00]
					],
					"smoothness":"interp",
					"beyond": "circle",
					"units" : {
						"arg1": "deg",
						"arg2": "deg",
						"table": "-"
					}
				},
				"X": {
					"title":"C_xht",
					"arg1": [-180.00,	-100.00, 80.00,	180.00],
					"arg1_title":"alpha_ht",
					"arg2": [-90.00,	-30.00,	30.00,	90.00],
					"arg2_title":"beta_ht",
					"table":[
					[0.0200E+00,	0.0200E+00,	0.0200E+00,	0.0200E+00],
					[2.0000E-03,	1.0000E-03,	1.1500E-01,	2.0000E-03],
					[1.1000E-02,	8.3300E-01,	8.7800E-01,	1.1000E-02],
					[0.0200E+00,  0.0200E+00,	0.0200E+00,	0.0200E+00]
					],
					"smoothness":"interp",
					"beyond": "circle",
					"units" : {
						"arg1": "deg",
						"arg2": "deg",
						"table": "-"
					}
				},
				"Z": null
			})";

	double X_arg1[] = { -180.00,	-100.00, 80.00,	180.00 };
	double X_arg2[] = { -90.00,	-30.00,	30.00,	90.00 };
	double X_table[4][4] = {
	    { 0.0200E+00,	0.0200E+00,	0.0200E+00,	0.0200E+00 },
	    { 2.0000E-03,	1.0000E-03,	1.1500E-01,	2.0000E-03 },
	    { 1.1000E-02,	8.3300E-01,	8.7800E-01,	1.1000E-02 },
	    { 0.0200E+00,  0.0200E+00,	0.0200E+00,	0.0200E+00 },
	};

	double Y_arg1[] = { -180.00,	-100.00, 80.00,	180.00 };
	double Y_arg2[] = { -90.00,	-30.00,	30.00,	90.00 };
	double Y_table[4][4] = {
	    { 0.0000E+00,	0.0000E+00,	0.0000E+00,	0.0000E+00 },
	{ 2.0000E-03,	1.0000E-03,	1.1500E-01,	2.0000E-03 },
	{ 1.1000E-02,	8.3300E-01,	8.7800E-01,	1.1000E-02 },
	{ 0.0000E+00,  0.0000E+00,	0.0000E+00,	0.0000E+00 },
	};

	T_VectorCombiTableDbl expected = {
	    PRESNC_VECTORCOMBITABLE_X | PRESNC_VECTORCOMBITABLE_Y | PRESNC_VECTORCOMBITABLE_Z,
	    // X
	    {
		{ const_cast<char*>("interp"),6,6 },
		{ const_cast<char*>("circle"),6,6 },

		{ X_arg1, sizeof(X_arg1) / sizeof(double) },
		{ const_cast<char*>("alpha_ht"),8,8 },

		{ X_arg2, sizeof(X_arg2) / sizeof(double) },
		{ const_cast<char*>("beta_ht"),7,7 },

		{ (double*)X_table, 4, 4 },
		{ const_cast<char*>("C_xht"),5,5 },
		{
		{ const_cast<char*>("deg"),3,3 },
		{ const_cast<char*>("deg"),3,3 },
		{ const_cast<char*>("-"),1,1 }
		}
	    },
	    // Y
	    {
		{ const_cast<char*>("interp"),6,6 },
	{ const_cast<char*>("circle"),6,6 },

	{ Y_arg1, sizeof(Y_arg1) / sizeof(double) },
	{ const_cast<char*>("alpha_ht"),8,8 },

	{ Y_arg2, sizeof(Y_arg2) / sizeof(double) },
	{ const_cast<char*>("beta_ht"),7,7 },

	{ (double*)Y_table, 4, 4 },
	{ const_cast<char*>("C_yht"),5,5 },
		{
		    { const_cast<char*>("deg"),3,3 },
	{ const_cast<char*>("deg"),3,3 },
	{ const_cast<char*>("-"),1,1 }
		}
	    },


	    // Z
	    {
	    },
	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	T_VectorCombiTableDbl o;
	JUST_DO_IT(test_parse_T_VectorCombiTableDbl(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_T_VectorCombiTableDbl_null_optional_fields()
    {
	const char* snippet = R"({
				"Y": null,
				"X": null,
				"Z": null
			})";

	T_VectorCombiTableDbl expected = {
	    PRESNC_VECTORCOMBITABLE_X | PRESNC_VECTORCOMBITABLE_Y | PRESNC_VECTORCOMBITABLE_Z,
	{}, {}, {}
	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	T_VectorCombiTableDbl o;
	JUST_DO_IT(test_parse_T_VectorCombiTableDbl(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_T_VectorCombiTableDbl_missing_optional_fields()
    {
	const char* snippet = R"({ })";

	T_VectorCombiTableDbl expected = { };

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	T_VectorCombiTableDbl o;
	JUST_DO_IT(test_parse_T_VectorCombiTableDbl(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_UnitsTwist()
    {
	const char* snippet = R"({
			"factor": "rad"
		})";
	UnitsTwist expected = {
	{ const_cast<char*>("rad"),3,3 },
	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	UnitsTwist o;
	JUST_DO_IT(test_parse_UnitsTwist(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_UnitsBladeSimple()
    {
	const char* snippet = R"({
			"r_0":			"m",
			"Length":		"m",
			"Chord07":	"m",
			"Mass":			"kg",
			"GravityMoment": 	"kg.m",
			"Inertia":				"kg.m2",
			"TipLossFactor": 	"-",
			"alpha_0":	"rad",
			"LiftStall":		"-",
			"LiftCurveSlope":	"rad-1",
			"Drag" : "-",
			"Induction" : "-",
			"Profile": "-",
			"LockNumber":			"?"
		})";

	UnitsBladeSimple expected = {
	    { const_cast<char*>("m"),1,1 },
	{ const_cast<char*>("m"),1,1 },
	{ const_cast<char*>("m"),1,1 },
	{ const_cast<char*>("kg"),2,2 },
	{ const_cast<char*>("kg.m"),4,4 },
	{ const_cast<char*>("kg.m2"),5,5 },
	{ const_cast<char*>("-"),1,1 },
	{ const_cast<char*>("rad"),3,3 },
	{ const_cast<char*>("rad-1"),5,5 },
	{ const_cast<char*>("-"),1,1 },
	{ const_cast<char*>("?"),1,1 },
	{ const_cast<char*>("-"),1,1 },
	{ const_cast<char*>("-"),1,1 },
	{ const_cast<char*>("-"),1,1 }
	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	UnitsBladeSimple o;
	JUST_DO_IT(test_parse_UnitsBladeSimple(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_T_param_blade_simple()
    {
	const char* snippet = R"({
				"guid":"{BBC6600A-712A-4F54-955F-52A8EFA13A5A}",
				"annotation" : "аннотация к blade",
				"$ref_units_": "#/_units_/_blade_",
				"flex_type":	0,
				"r_0":			0.8,
				"Length":		3.5,
				"Chord07":	0.44,
				"Mass":			95.00,
				"GravityMoment": 425.90,
				"Inertia":	2200.0,
				"Twist":{
					"$ref_units_": "#/_units_/_twist_",
					"type":"tip-linear",
					"factor": -0.06
				},
				"TipLossFactor":	0.96,
				"alpha_0":	-0.01,
				"LiftCurveSlope":	5.8,
				"LiftStall":			1.3,
				"Drag":						null,
				"Induction":      1.0,
				"Profile":				5.0,
				"LockNumber":			4.9
			})";
	T_param_blade_simple expected = { 
		0xFFFF,
		0,
		0.8,
		3.5,
		0.44,
		95.00,
		425.90,
		2200.0,
	{ 
	    -0.06,
	    { const_cast<char*>("tip-linear"),10,10 },
	    {} // ref unit
	},
	0.96,
	5.8,
	1.3,
	-0.01,
	4.9,
		0,
		1.0,
		5.0,
	{} // ref unit

	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	T_param_blade_simple o;
	JUST_DO_IT(test_parse_T_param_blade_simple(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_T_param_blade_simple_null_optional_fields()
    {
	const char* snippet = R"({
				"guid":"{BBC6600A-712A-4F54-955F-52A8EFA13A5A}",
				"annotation" : "аннотация к blade",
				"$ref_units_": "#/_units_/_blade_",
				"flex_type":	null,
				"r_0":			null,
				"Length":		null,
				"Chord07":	null,
				"Mass":			null,
				"GravityMoment": null,
				"Inertia":	null,
				"Twist":null,
				"TipLossFactor":	null,
				"alpha_0":	null,
				"LiftCurveSlope":	null,
				"LiftStall":			null,
				"Drag":						null,
				"Induction":      null,
				"Profile":				null,
				"LockNumber":			null
			})";
	T_param_blade_simple expected;
	memset(&expected, 0, sizeof(expected));
	expected.presnc = 0xFFFF;
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	T_param_blade_simple o;
	JUST_DO_IT(test_parse_T_param_blade_simple(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_T_param_blade_simple_missing_optional_fields()
    {
	const char* snippet = R"({
				"guid":"{BBC6600A-712A-4F54-955F-52A8EFA13A5A}",
				"annotation" : "аннотация к blade",
				"$ref_units_": "#/_units_/_blade_"
			})";
	T_param_blade_simple expected;
	memset(&expected, 0, sizeof(expected));
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	T_param_blade_simple o;
	JUST_DO_IT(test_parse_T_param_blade_simple(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_T_param_enginewithregulator()
    {
	const char* snippet = R"({
  "n" : 1, 
  "rpm_pace_accel" : 2,
  "rpm_pace_decel" : 3,
  "t1" : 4, 
  "t2" : 5,
  "om1" : 6,
  "om2" : 7,
  "poly2_rpm_power" : {  "a0" : 8, "a1" : 9, "a2" : 10  },
  "power_hbar_temp_tbl" : null
})";
	T_param_enginewithregulator expected = {
		1,2,3,4,5,6,7,{8,9,10}, {}

	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	T_param_enginewithregulator o;
	JUST_DO_IT(test_parse_T_param_enginewithregulator(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
	////////////////////////////////////
    void test_UnitsTransmission1M1T() {
	const char* snippet = R"({
			"PowerLimit":	"h.p.",
			"PowerBoardLosses": "kW",
			"PowerMainUse": "1",
			"PowerTailUse": "1"
		})";
	UnitsTransmission1M1T expected = {
	    { const_cast<char*>("h.p."),4,4 },
	{ const_cast<char*>("kW"),2,2 },
	{ const_cast<char*>("1"),1,1 },
	{ const_cast<char*>("1"),1,1 }
	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	UnitsTransmission1M1T o;
	JUST_DO_IT(test_parse_UnitsTransmission1M1T(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_T_param_transmission_1m1t_rotor() {
	const char* snippet = R"({
			"$ref_units_": "#/_units_/_transmission_",
			"PowerLimit":	1160.0,
			"PowerBoardLosses": 59.0,
			"PowerMainUse": 0.96,
			"PowerTailUse": 0.94
		})";
	T_param_transmission_1m1t_rotor expected = { 1160.0, 59.0, 0.96,  0.94, {} };
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	T_param_transmission_1m1t_rotor o;
	JUST_DO_IT(test_parse_T_param_transmission_1m1t_rotor(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_UnitsHingeGeneral() {
	const char* snippet = R"({
			"stop":	"deg",
			"angle":	"deg",
			"L_offset":	"m",
			"damping":	"?",
			"stiffness":	"N.m.rad-1"
		})";
	UnitsHingeGeneral expected = {
	    { const_cast<char*>("deg"),3,3 },
	{ const_cast<char*>("deg"),3,3 },
	{ const_cast<char*>("m"),1,1 },
	{ const_cast<char*>("?"),1,1 },
	{ const_cast<char*>("N.m.rad-1"),9,9 },
	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	UnitsHingeGeneral o;
	JUST_DO_IT(test_parse_UnitsHingeGeneral(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_T_param_hinge_general() {
	const char* snippet = R"({
					"$ref_units_": "#/_units_/_hinge_",
					"fix_cond":	0,
					"stop_min":	-6.0,
					"stop_max":	23.0,
					"angle_0":		-0.01,
					"L_offset":		0.15,
					"damping":		0.0,
					"stiffness":	0.0
				})";

	T_param_hinge_general expected = {
		0x7F,
		0,
		-6.0,
		23.0,
		-0.01,
		0.15,
		0.0,
		0.0,
	{} // ref 
	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	T_param_hinge_general o;
	JUST_DO_IT(test_parse_T_param_hinge_general(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_T_param_hinge_general_null_optional_fields() {
	const char* snippet = R"({
					"$ref_units_": "#/_units_/_hinge_",
					"fix_cond":	null,
					"stop_min":	null,
					"stop_max":	null,
					"angle_0":		null,
					"L_offset":		null,
					"damping":		null,
					"stiffness":	null
				})";

	T_param_hinge_general expected = {0x7F,0,0,0,0,0,0,0,{}	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	T_param_hinge_general o;
	JUST_DO_IT(test_parse_T_param_hinge_general(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_T_param_hinge_general_missing_optional_fields() {
	const char* snippet = R"({
					"$ref_units_": "#/_units_/_hinge_"
				})";

	T_param_hinge_general expected = { 0,0,0,0,0,0,0,0,{} };
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	T_param_hinge_general o;
	JUST_DO_IT(test_parse_T_param_hinge_general(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }

    void test_T_param_hub_simple() {
	const char* snippet = R"({
				"guid":"{2B44A09D-4B41-4F80-B5FF-2EB75B85F8BC}",
				"$ref_units_": "#/_units_/_hub_",
				"NumBlades": 3,
				"Type":			"3-hinged",
				"Flap_hinge":{
					"$ref_units_": "#/_units_/_hinge_",
					"fix_cond":	0,
					"stop_min":	null,
					"stop_max":	null,
					"angle_0":		0.0,
					"L_offset":		0.15,
					"damping":		0.0,
					"stiffness":	0.0
				},
				"Feather_hinge": null,
				"Inertia": null
			})";

	T_param_hub_simple expected = {
	    HUBSIMLPE_INERTIA | HUBSIMLPE_FLAP_HINGE /*| HUBSIMPLE_LAG_HINGE*/ | HUBSIMPLE_FEATHER_HINGE,
	    3,
	    0,
		// flap_hinge
	    {
	    	HINGE_FIX | HINGE_STOPMIN | HINGE_STOPMAX | HINGE_ANGLE0  | HINGE_LOFFSET | HINGE_DAMPING | HINGE_STIFFNESS , 
	    	0,0,0,0, 0.15, 0,0, 
	    	{}
	    },
		// lag_hinge
	    {},
		// feather_hinge
	    {},
	    {}
	};
	expected.feather_hinge.presnc = HINGE_FIX | HINGE_STOPMIN | HINGE_STOPMAX | HINGE_ANGLE0 | HINGE_LOFFSET | HINGE_DAMPING | HINGE_STIFFNESS;
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	T_param_hub_simple o;
	JUST_DO_IT(test_parse_T_param_hub_simple(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_UnitsSwashplate() {
	const char* snippet = R"({
			"D1":"1",
			"D2":"1",
			"lon_max": "deg",
			"lon_min": "deg",
			"lat_max": "deg",
			"lat_min": "deg"
		})";

	UnitsSwashplate expected = {
	    { const_cast<char*>("1"),1,1 },
	    { const_cast<char*>("1"),1,1 },
	    { const_cast<char*>("deg"),3,3 },
	    { const_cast<char*>("deg"),3,3 },
	    { const_cast<char*>("deg"),3,3 },
	    { const_cast<char*>("deg"),3,3 }
	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	UnitsSwashplate o;
	JUST_DO_IT(test_parse_UnitsSwashplate(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_T_param_swashplate() {
	const char* snippet = R"({
				"annotation":	"аннотация к Swashplate",
				"$ref_units_": "#/_units_/_swashplate_",
				"D1":	2.8,
				"D2":	0.3,
				"lon_max": 5.0,
				"lon_min": -7.5,
				"lat_max": 4.2,
				"lat_min": -4.0
			})";
	T_param_swashplate expected = {
	    2.8,
	    0.3,
	    5.0,
	    -7.5,
	    4.2,
	    -4.0,
	    {}
	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	T_param_swashplate o;
	JUST_DO_IT(test_parse_T_param_swashplate(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_T_param_cet_1m1t_rotor() {
	const char* snippet = R"({
  "ct_0": null, 
  "ct_oz": null, 
  "ct_ox": null, 

  "ch_0": null, 
  "ch_oz": null, 
  "ch_ox": null, 

  "cs_0": null, 
  "cs_oz": null, 
  "cs_ox": null, 

  "cmx_0": null, 
  "cmx_oz": null, 
  "cmx_ox": null, 

  "cmz_0": null, 
  "cmz_oz": null, 
  "cmz_ox": null, 

  "cmk_0": null, 
  "cmk_oz": null, 
  "cmk_ox": null, 
  "cmk_om": null, 

  "tr_ct": null, 
  "tr_ch": null, 
  "tr_cs": null, 
  "tr_cmk": null, 
  "Omg_con": null
})";
	T_param_cet_1m1t_rotor expected = {};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	T_param_cet_1m1t_rotor o;
	JUST_DO_IT(test_parse_T_param_cet_1m1t_rotor(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_UnitsRotorSimple() {
	const char* snippet = R"({
			"Radius":	"m",
			"Omega_nom": "rad.s-1",
			"Omega_min": "rad.s-1",
			"OmegaRatio":"1",
			"Direction": "-",
			"PitchFlapCoupling": "1"
		})";

	UnitsRotorSimple expected = {
	    { const_cast<char*>("-"),1,1 },
	{ const_cast<char*>("m"),1,1 },
	{ const_cast<char*>("1"),1,1 },
	{ const_cast<char*>("rad.s-1"),7,7 },
	{ const_cast<char*>("rad.s-1"),7,7 },
	{ const_cast<char*>("1"),1,1 },
	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	UnitsRotorSimple o;
	JUST_DO_IT(test_parse_UnitsRotorSimple(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_T_param_rotor_simple() {
	const char* snippet = u8R"({
			"guid":"{04358EE5-D7CA-4D5A-B4B6-31F93BD4EE03}",
			"$ref_units_": "#/_units_/_rotor_",
			"Type": "тип тэйл ротора",
			"Radius":			1.00,
			"Omega_nom": 	null,
			"Omega_min": 	null,
			"OmegaRatio":	7.86,
			"Direction": 	1,
			"PitchFlapCoupling": 1.0,
			"Hub": null,
			"Swashplate": null,
			"Blade": null,
			"MeanProfileDrag":null,
			"AeroDataGrid": null
		})";

	T_param_rotor_simple expected = {
PRESNC_ROTORSIMPLE_DIRECTION | PRESNC_ROTORSIMPLE_R  | PRESNC_ROTORSIMPLE_PITCHFLAPCOUPLING | PRESNC_ROTORSIMPLE_OMEGA_NOM |
PRESNC_ROTORSIMPLE_OMEGA_MIN | PRESNC_ROTORSIMPLE_OMEGA_RATIO | PRESNC_ROTORSIMPLE_MEANPROFILEDRAG | PRESNC_ROTORSIMPLE_SWASHPLATE,
	    1,
	    1.00,
	    1.0,
	    0,
	    0,
	    7.86,
	{ const_cast<char*>(u8"тип тэйл ротора"),15,28 },
		// hub
	{},
		// blade
	{},
		// swashplate
	{},
		// mean_profile_drag
	{},
		// ref units
	{}
	};
	expected.hub.presnc = 0xF;
	expected.blade.presnc = 0xFFFF;
	expected.hub.flap_hinge.presnc =
    	expected.hub.lag_hinge.presnc =
    	expected.hub.feather_hinge.presnc = HINGE_FIX | HINGE_STOPMIN | HINGE_STOPMAX | HINGE_ANGLE0 | HINGE_LOFFSET | HINGE_DAMPING | HINGE_STIFFNESS;

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	T_param_rotor_simple o;
	JUST_DO_IT(test_parse_T_param_rotor_simple(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_UnitsHTail() {
	const char* snippet = R"({
			"Area": "m2",
			"Span": "m",
			"V_shape": "deg"
		})";
	UnitsHTail expected = {
	    { const_cast<char*>("m"),1,1 },
	{ const_cast<char*>("deg"),3,3 },
	{ const_cast<char*>("m2"),2,2 }
	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	UnitsHTail o;
	JUST_DO_IT(test_parse_UnitsHTail(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_T_param_H_tail() {
	const char* snippet = R"({
			"guid":"{878C5764-CD8E-419F-AE6C-70F1C4CA9043}",
			"$ref_units_": "#/_units_/_h_tail_",
			"Area": 1.0,
			"V_shape": 0.0,
			"Span" : 2.0,
			"Excentric": 0.0,
			"CenterOfPressure" : [0.0, 0.0, 0.0],
			"Position" : null,
			"Kinematics":[0.0, 0.0, 0.065],
			"AeroForceCoeffs": {
				"Y": null,
				"X": null,
				"Z": null
			}
		})";

	T_param_H_tail expected = {
	    PRESNC_HTAIL_SPAN | PRESNC_HTAIL_V_SHAPE | PRESNC_HTAIL_AREA | PRESNC_HTAIL_EXCENTR 
	  | PRESNC_HTAIL_C_PRESS |  PRESNC_HTAIL_KINEMATICS | PRESNC_HTAIL_POSITION | PRESNC_HTAIL_AEROFORCE /*| PRESNC_HTAIL_AEROMOMENT*/
	  ,
	    2.0,
	    0.0,
	    1.0,
	    0.0,
	    {},
	    {0.0, 0.0, 0.065},
	    // position
	    {},
	    // AeroForceCoeffs
	    {},
	    // aero_moment_coeffs
	    {},
	    {}
	};

	expected.aero_force_coeffs.presnc = PRESNC_VECTORCOMBITABLE_X | PRESNC_VECTORCOMBITABLE_Y | PRESNC_VECTORCOMBITABLE_Z;

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	T_param_H_tail o;
	JUST_DO_IT(test_parse_T_param_H_tail(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_UnitsVTail() {
	const char* snippet = R"({
			"Area": "m2",
			"V_shape": "deg"
		})";
	UnitsVTail expected = {
	{ const_cast<char*>("deg"),3,3 },
	{ const_cast<char*>("m2"),2,2 }
	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	UnitsVTail o;
	JUST_DO_IT(test_parse_UnitsVTail(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_T_param_V_tail() {
	const char* snippet = R"({
			"guid":"{4BDF3FFE-E42D-480F-96CC-D40E3251E28B}",
			"$ref_units_": "#/_units_/_v_tail_",
			"NSurfaces": 1,
			"SymmetryPlane": "XY",
			"Area":	1.4,
			"AreaUnderFlow": 1.4,
			"V_shape": 0.0,
			"CenterOfPressure" : [0.0, 0.0, 0.0],
			"Position" : null
		})";
/*
short presnc;
short nsurfs;
char symmplane[3];
double  area;
double  area_flow;
double  v_shape;
double  c_press[3];
T_position  position;
T_VectorCombiTableDbl aero_force_coeffs;
UnitsVTail  units;
 */
	T_param_V_tail expected = {
  PRESNC_VTAIL_SYMMPLANE | PRESNC_VTAIL_V_SHAPE | PRESNC_VTAIL_AREA | PRESNC_VTAIL_AREA_FLOW
		| PRESNC_VTAIL_C_PRESS | PRESNC_VTAIL_POSITION  /*| PRESNC_VTAIL_AEROFORCE */
		,
		1,
		{ 'X', 'Y', 0 },
		1.4,
		1.4,
		0.0,
		{},
		{},
		{},
		{}
	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	T_param_V_tail o;
	JUST_DO_IT(test_parse_T_param_V_tail(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_UnitsFuselageSimple() {
	const char* snippet = R"({
			"Length": "m",
			"CrossArea": "m2",
			"Inertia": "kg.m2"
		})";
	UnitsFuselageSimple expected = {
	    { const_cast<char*>("m"),1,1 },
	    { const_cast<char*>("m2"),2,2 },
	    { const_cast<char*>("kg.m2"),5,5 }
	};
	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	UnitsFuselageSimple o;
	JUST_DO_IT(test_parse_UnitsFuselageSimple(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_T_param_fuselage_simple() {
	const char* snippet = R"({
			"guid":"{3F6F360D-8FEA-4FCF-98BF-547E46EE9E16}",
			"$ref_units_": "#/_units_/_body_",
			"Type": "Тип аппарата",
			"Length": 10.5,
			"CrossArea": 3.7,
			"Inertia":null,
			"AeroForceCoeffs": null,
			"AeroMomentCoeffs":null
		})";
/*
double          length;
double          cross_area;
T_inertia       inertia;
T_VectorCombiTableDbl aero_force_coeffs;
T_VectorCombiTableDbl aero_moment_coeffs;
UnitsFuselageSimple   units; 
 */

	T_param_fuselage_simple expected = {
	    10.5,
	    3.7,
	    {},
	    {},
	    {},
	    {}
	};
	expected.aero_force_coeffs.presnc = PRESNC_VECTORCOMBITABLE_X | PRESNC_VECTORCOMBITABLE_Y | PRESNC_VECTORCOMBITABLE_Z;
	expected.aero_moment_coeffs.presnc = PRESNC_VECTORCOMBITABLE_X | PRESNC_VECTORCOMBITABLE_Y | PRESNC_VECTORCOMBITABLE_Z;

	print(__FUNCTION__);
	JUST_DO_IT(setup_snippet(snippet));
	T_param_fuselage_simple o;
	JUST_DO_IT(test_parse_T_param_fuselage_simple(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
    void test_T_param_simple_one_rotor_helicopter() {
	const char* snippet = R"({
		"guid":"{143AB745-6977-48ED-AA1C-96AC83FAB45D}",
		"$ref_units_":"#/_units_",
		"TakeOffWeight": 5000.0,
		"CenterOfGravity": [0.20, 0.0, 0.0],
		"Body": null,
		"MainRotor":null,
		"MainRotorPosition":null,
		"TailRotor":null,
		"TailRotorPosition":null,
		"Transmission": null,
		"H_Tail": null,
		"Body2HTailFlowDeclination": null,
		"Wing2HTailFlowDeclination": null,
		"V_Tail": null,
		"Wing": null,
		"OnBodyFlowLosses" : 0
	})";
	/*
	double          TOW;
	double          CG[3];
	T_param_fuselage_simple Body;
	T_param_rotor_simple    MainRotor;
	T_position              MainRotor_Position;
	T_param_rotor_simple    TailRotor;
	T_position              TailRotor_Position;
	T_param_transmission_1m1t_rotor Transmission;
	T_param_H_tail          HTail;
	T_param_V_tail          VTail;
	double                  OnBodyFlowLosses;
	 */
	T_param_simple_one_rotor_helicopter expected = {
	    5000.0,
	    {0.20, 0.0, 0.0},
		// Body
	    {},
	{}, {},
	{},{},
	{},
	{},{},
	0
	};
	print(__FUNCTION__);

	expected.Body.aero_force_coeffs.presnc = 7;
	expected.Body.aero_moment_coeffs.presnc = 7;

	expected.MainRotor.presnc = 0xFF;
	expected.MainRotor.blade.presnc = 0xFFFF;
	expected.MainRotor.hub.presnc = 0xF;
	expected.MainRotor.hub.flap_hinge.presnc = 0x7F;
	expected.MainRotor.hub.lag_hinge.presnc = 0x7F;
	expected.MainRotor.hub.feather_hinge.presnc = 0x7F;

	expected.TailRotor.presnc = 0xFF;
	expected.TailRotor.blade.presnc = 0xFFFF;
	expected.TailRotor.hub.presnc = 0xF;
	expected.TailRotor.hub.flap_hinge.presnc = 0x7F;
	expected.TailRotor.hub.lag_hinge.presnc = 0x7F;
	expected.TailRotor.hub.feather_hinge.presnc = 0x7F;

	expected.HTail.presnc = 0x1FF;
	expected.HTail.aero_force_coeffs.presnc = 7;
	expected.HTail.aero_moment_coeffs.presnc = 7;

	expected.VTail.presnc = 0xDF;
	expected.VTail.aero_force_coeffs.presnc = 7;

	JUST_DO_IT(setup_snippet(snippet));
	T_param_simple_one_rotor_helicopter o;
	JUST_DO_IT(test_parse_T_param_simple_one_rotor_helicopter(parser, &o));

	if (!(o == expected))
	    goto error_handler;

	test_ok();
	return;
    error_handler:
	test_fail();
	return;
    }
};



int main(int argc, char** argv)
{
    if (argc < 2)
    {
	std::cout << "usage param1: <path to json file>"<< endl 
    	<<" param2: boolean verbose(\"0\"-disable verbose mode, \"1\"-enable verbose mode" << endl;
	return 0;
    }
    test_parse_example_json(argc, argv);
    {
	tests_t tests;

	tests.test_EString();

	tests.test_array_dbl_1x3();
	tests.test_array_dbl_1x3_null();
	tests.test_array_dbl_1x3_non_array();
	tests.test_array_dbl_1x3_size_mismatch();
	tests.test_array_dbl_1x3_all_elements_null();
	tests.test_array_dbl_1x3_element_type_mismatch();

	tests.test_array_dbl_3x3();
	tests.test_array_dbl_3x3_all_integers();
	tests.test_array_dbl_3x3_null();
	tests.test_array_dbl_3x3_all_subarrays_null();
	tests.test_array_dbl_3x3_size_mismatch();
	tests.test_array_dbl_3x3_non_array();

	tests.test_array_char_1x3();
	tests.test_array_char_1x3_null();
	tests.test_array_char_1x3_non_string();
	tests.test_array_char_1x3_char_count_gt_3();
	tests.test_array_char_1x3_byte_count_gt_3();
	tests.test_array_char_1x3_size_lt_3();

	tests.test_EArrayDbl();
	tests.test_EArrayDbl_null();
	tests.test_EArrayDbl_empty();
	tests.test_EArrayDbl_non_array();
	tests.test_EArrayDbl_heterogeneous();
	tests.test_EArrayDbl_elements_all_null();
	tests.test_EArrayDbl_element_type_mismatch();

	tests.test_ETableDbl();
	tests.test_ETableDbl_null();
	tests.test_ETableDbl_empty();
	tests.test_ETableDbl_all_subarrays_null();
	tests.test_ETableDbl_diff_size_subarrays_expected_fail();
	tests.test_ETableDbl_null_subarray();
	tests.test_ETableDbl_subarray_non_array();
	tests.test_ETableDbl_all_subarray_empty();
	tests.test_ETableDbl_all_elements_null();
	tests.test_ETableDbl_element_type_mismatch();

	tests.test_ERank3ArrayDbl();
	tests.test_ERank3ArrayDbl_null();
	tests.test_ERank3ArrayDbl_non_array();
	tests.test_ERank3ArrayDbl_empty();
	tests.test_ERank3ArrayDbl_subarray_non_array();
	tests.test_ERank3ArrayDbl_subsubarray_non_array();
	tests.test_ERank3ArrayDbl_subsubarray_size_mismatch();
	tests.test_ERank3ArrayDbl_subarray_size_mismatch();
	tests.test_ERank3ArrayDbl_all_subsubarrays_null();
	tests.test_ERank3ArrayDbl_all_subarrays_null();
	tests.test_ERank3ArrayDbl_element_non_number();
	tests.test_ERank3ArrayDbl_null_subarray();
	tests.test_ERank3ArrayDbl_null_subsubarray();
	tests.test_ERank3ArrayDbl_all_elements_null();

	tests.test_EPoly2Dbl();
	tests.test_UnitsInertia();
	tests.test_UnitsCombiTable();
	tests.test_UnitsCombi3Table();
	tests.test_CombiTableDbl();
	tests.test_Combi3TableDbl();
	tests.test_UnitsPosition();
	tests.test_T_position();
	tests.test_T_factor();
	tests.test_T_VectorCombiTableDbl();
	tests.test_T_VectorCombiTableDbl_missing_optional_fields();
	tests.test_T_VectorCombiTableDbl_null_optional_fields();
	tests.test_UnitsTwist();

	tests.test_UnitsBladeSimple();
	tests.test_T_param_blade_simple();
	tests.test_T_param_blade_simple_null_optional_fields();
	tests.test_T_param_blade_simple_missing_optional_fields();

	tests.test_T_param_enginewithregulator();

	tests.test_UnitsTransmission1M1T();
	tests.test_T_param_transmission_1m1t_rotor();

	tests.test_UnitsHingeGeneral();
	tests.test_T_param_hinge_general();
	tests.test_T_param_hinge_general_null_optional_fields();
	tests.test_T_param_hinge_general_missing_optional_fields();

	tests.test_T_param_hub_simple();

	tests.test_UnitsSwashplate();
	tests.test_T_param_swashplate();

	tests.test_T_param_cet_1m1t_rotor();

	tests.test_UnitsRotorSimple();
	tests.test_T_param_rotor_simple();

	tests.test_UnitsHTail();
	tests.test_T_param_H_tail();

	tests.test_UnitsVTail();
	tests.test_T_param_V_tail();

	tests.test_UnitsFuselageSimple();
	tests.test_T_param_fuselage_simple();

	tests.test_T_param_simple_one_rotor_helicopter();



    std::cout << "PASSED: " << tests.passed << " FAILED: " << tests.failed << endl;
    }


    return 0;
}
